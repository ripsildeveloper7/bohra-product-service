'use strict';

var wishListMgr = require('./wishListMgr');


module.exports = function (app) {
  app.route('/wishlistmvp/:userId') 
    .put(wishListMgr.addWishList);
  app.route('/wishlistmvp/:userId/:id') 
    .delete(wishListMgr.removeWishList);
  app.route('/movetoaddtocartmvp/:userId') 
    .put(wishListMgr.moveToAddToCart);
    app.route('/movetowishlistmvp/:userId') 
    .put(wishListMgr.moveToWishList);
    app.route('/getwishlistmvp/:userId') 
    .get(wishListMgr.getWishList);
}