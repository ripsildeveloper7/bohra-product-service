var Color = require('../model/colorSetting.model');
var FilterOption = require('../model/filter-option.model');
var Note = require('../model/categoryNote.model');


exports.getColorSetting = function (req, res) {
  Color.find({}).select().exec(function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(data);
    }
  });
}



exports.getSingleProductColor = function (req, res) {
  Color.find({ _id: { $in: req.body.ids } }).select().exec(function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(data);
    }
  });
}

exports.addColorSetting = function (req, res) {
  var color = new Color();
  color.colorName = req.body.colorName;
  // color.colorCode = req.body.colorCode;
  color.colorChoose = req.body.colorChoose;
  color.save(function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      Color.find({}).select().exec(function (err, data) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          res.status(200).json(data);
        }
      });
    }
  });
}
exports.deleteColorSetting = function (req, res) {
  Color.findByIdAndRemove(req.params.id, function (err) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      Color.find({}).select().exec(function (err, allColor) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          res.status(200).json(allColor);
        }
      });
    }
  });
}

exports.addFilterPriceOptions = function (req, res) {
  /*  FilterOption.find({}, function (err, filterPrice) {
     if (err) {
       res.status(500).send({
         "result": 0
       });
     } else {
       if(filterPrice[0]){
         let price = {
           maxPrice: req.body.maxPrice,
           minPrice: req.body.minPrice
         };
         FilterOption.findOneAndUpdate({
           "filterOptions.filterOptionPriceName": req.body.filterOptionPriceName
           }, {
             $push: {
               "filterOptions.$.filterOptionPriceValue": price
             }
           },
           function (err, pricefilterOptions) {
             if (err) { // if it contains error return 0
               res.status(500).send({
                 "result": 0
               });
             } else {
               res.status(200).json(pricefilterOptions);     
             }
           });
       } else {
         var filterPrice = new FilterOption();
         let price = {
           maxPrice: req.body.maxPrice,
           minPrice: req.body.minPrice
         };
         var totalPrice = [];
         var allPriceDetails = []
         totalPrice.push(price);
         allPriceDetails.push({filterOptionPriceName: req.body.filterOptionPriceName,
           filterOptionPriceValue: totalPrice});
         filterPrice.filterOptions = allPriceDetails;
         filterPrice.save(function (err, data) {
           if (err) {
             res.status(500).send({
               message: "Some error occurred while retrieving notes."
             });
           } else {
             FilterOption.find({}).select().exec(function (err, data) {
               if (err) {
                 res.status(500).send({
                   message: "Some error occurred while retrieving notes."
                 });
               } else {
                 res.status(200).json(data);
               }
             });
           }
         }); */
  FilterOption.find({}, function (err, filterPrice) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      if (filterPrice[0]) {
        let price = {
          maxPrice: req.body.maxPrice,
          minPrice: req.body.minPrice
        };
        FilterOption.findOneAndUpdate({
          _id: filterPrice[0]._id
        }, {
          $push: {
            "filterOptionPriceValue": price
          }
        },
          function (err, pricefilterOptions) {
            if (err) { // if it contains error return 0
              res.status(500).send({
                "result": 0
              });
            } else {
              FilterOption.find({}).select().exec(function (err, data) {
                if (err) {
                  res.status(500).send({
                    message: "Some error occurred while retrieving notes."
                  });
                } else {
                  res.status(200).json(data);
                }
              });
            }
          });
      } else {
        var filterPrice = new FilterOption();
        let price = {
          maxPrice: req.body.maxPrice,
          minPrice: req.body.minPrice
        };
        var totalPrice = [];
        totalPrice.push(price);
        filterPrice.filterOptionPriceValue = totalPrice;
        filterPrice.save(function (err, data) {
          if (err) {
            res.status(500).send({
              message: "Some error occurred while retrieving notes."
            });
          } else {
            FilterOption.find({}).select().exec(function (err, data) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(data);
              }
            });
          }
        });
      }
    }
  });
}


exports.addFilterDiscountOptions = function (req, res) {
  FilterOption.find({}, function (err, filterDiscount) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      if (filterDiscount[0]) {
        let discount = {
          maxDiscount: req.body.maxDiscount,
          minDiscount: req.body.minDiscount
        };
        FilterOption.findOneAndUpdate({
          _id: filterDiscount[0]._id
        }, {
          $push: {
            "filterOptionDiscountValue": discount
          }
        },
          function (err, discountfilterOptions) {
            if (err) { // if it contains error return 0
              res.status(500).send({
                "result": 0
              });
            } else {
              FilterOption.find({}).select().exec(function (err, data) {
                if (err) {
                  res.status(500).send({
                    message: "Some error occurred while retrieving notes."
                  });
                } else {
                  res.status(200).json(data);
                }
              });
            }
          });
      } else {
        var filterDiscount = new FilterOption();
        let discount = {
          maxDiscount: req.body.maxDiscount,
          minDiscount: req.body.minDiscount
        };
        var totalDiscount = [];
        totalDiscount.push(discount);
        filterDiscount.filterOptionDiscountValue = discount;
        filterDiscount.save(function (err, data) {
          if (err) {
            res.status(500).send({
              message: "Some error occurred while retrieving notes."
            });
          } else {
            FilterOption.find({}).select().exec(function (err, data) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(data);
              }
            });
          }
        });
      }
    }
  });
}



exports.addFilterDispatchOptions = function (req, res) {
  FilterOption.find({}, function (err, filterDispatch) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      if (filterDispatch[0]) {
        let dispatch = {
          day: req.body.day
        };
        FilterOption.findOneAndUpdate({
          _id: filterDispatch[0]._id
        }, {
          $push: {
            "filterDispatchValue": dispatch
          }
        },
          function (err, dispatchfilterOptions) {
            if (err) { // if it contains error return 0
              res.status(500).send({
                "result": 0
              });
            } else {
              FilterOption.find({}).select().exec(function (err, data) {
                if (err) {
                  res.status(500).send({
                    message: "Some error occurred while retrieving notes."
                  });
                } else {
                  res.status(200).json(data);
                }
              });
            }
          });
      } else {
        var filterDispatch = new FilterOption();
        let dispatch = {
          day: req.body.day
        };
        var totalDispatch = [];
        totalDispatch.push(dispatch);
        filterDispatch.filterOptionDispatchValue = dispatch;
        filterDiscount.save(function (err, data) {
          if (err) {
            res.status(500).send({
              message: "Some error occurred while retrieving notes."
            });
          } else {
            FilterOption.find({}).select().exec(function (err, data) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(data);
              }
            });
          }
        });
      }
    }
  });
}


exports.deleteFilterPrice = function (req, res) {
  FilterOption.findOneAndUpdate({_id: req.params.id }, {
      $pull: {
          'filterOptionPriceValue': {
            _id: req.params.priceId
          }
      }
  
  }, function (err, data) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      FilterOption.find({}).select().exec(function (err, data) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          res.status(200).json(data);
        }
      })
  }
});
}

exports.deleteFilterDiscount = function (req, res) {
  FilterOption.findOneAndUpdate({_id: req.params.id }, {
      $pull: {
          'filterOptionDiscountValue': {
            _id: req.params.discountId
          }
      }
  
  }, function (err, data) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      FilterOption.find({}).select().exec(function (err, data) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          res.status(200).json(data);
        }
      })
  }
});
}


exports.deleteFilterDispatch = function (req, res) {
  FilterOption.findOneAndUpdate({_id: req.params.id }, {
      $pull: {
          'filterDispatchValue': {
            _id: req.params.dispatchId
          }
      }
  
  }, function (err, data) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      FilterOption.find({}).select().exec(function (err, data) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          res.status(200).json(data);
        }
      })
  }
});
}

exports.findfilterOptions = function (req, res) {
  FilterOption.find({}).select().exec(function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(data);
    }
  })
}

exports.createNote = function (req, res) {
  var note = new Note();
  note.superCategoryId = req.body.superCategoryId;
  note.superCategoryName = req.body.superCategoryName;
  note.subCategoryId = req.body.subCategoryId;
  note.subCategoryName = req.body.subCategoryName;
  note.note = req.body.note;
  note.save(function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      Note.find({}).select().exec(function (err, data) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          res.status(200).json(data);
        }
      });
    }
  });
}

exports.deleteNote = function (req, res) {
  Note.findByIdAndRemove(req.params.id, function (err) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      Note.find({}).select().exec(function (err, note) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          res.status(200).json(note);
        }
      });
    }
  });
}

exports.getNote = function (req, res) {
  Note.find({}).select().exec(function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(data);
    }
  });
}

exports.editNote = function (req, res) {
  Note.findOneAndUpdate({
    "_id": req.params.id
  }, {
  note: req.body.note
  }).select().exec(function (err, data) {
    if (err) {
      res.status(500).json(err);
    } else {
      Note.findOne({
        _id: req.params.id
      }).select().exec(function (err, editNoteData) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          res.status(200).json(editNoteData);
        }
      });
    }
  });
}
