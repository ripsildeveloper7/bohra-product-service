
var productSettingDA = require('./productSettingDA');

exports.getColorSetting = function (req, res) {
  try {
    productSettingDA.getColorSetting(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.addColorSetting = function (req, res) {
    try {
      productSettingDA.addColorSetting(req, res);
    } catch (error) {
      console.log(error);
    }
  }

  
exports.deleteColorSetting = function (req, res) {
  try {
    productSettingDA.deleteColorSetting(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.deleteFilterPrice = function (req, res) {
  try {
    productSettingDA.deleteFilterPrice(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.deleteFilterDiscount = function (req, res) {
  try {
    productSettingDA.deleteFilterDiscount(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.deleteFilterDispatch = function (req, res) {
  try {
    productSettingDA.deleteFilterDispatch(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getSingleProductColor = function (req, res) {
  try {
    productSettingDA.getSingleProductColor(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.addFilterPriceOptions = function (req, res) {
  try {
    productSettingDA.addFilterPriceOptions(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.addFilterDiscountOptions = function (req, res) {
  try {
    productSettingDA.addFilterDiscountOptions(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.addFilterDispatchOptions = function (req, res) {
  try {
    productSettingDA.addFilterDispatchOptions(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.findfilterOptions = function (req, res) {
  try {
    productSettingDA.findfilterOptions(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.createNote = function (req, res) {
  try {
    productSettingDA.createNote(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.deleteNote = function (req, res) {
  try {
    productSettingDA.deleteNote(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.getNote = function (req, res) {
  try {
    productSettingDA.getNote(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.editNote = function ( req, res){
  try{
    productSettingDA.editNote(req,res);
  } catch (error){
    console.log(error);
  }
  }

