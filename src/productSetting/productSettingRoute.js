'use strict';
var productSettingMgr = require('./productSettingMgr');
module.exports = function (app) {
    app.route('/getcolorsetting')
        .get(productSettingMgr.getColorSetting);
    app.route('/addcolorsetting')
        .post(productSettingMgr.addColorSetting);
    app.route('/deletecolorsetting/:id')
        .delete(productSettingMgr.deleteColorSetting);
    app.route('/getcolorssingleproduct')
        .post(productSettingMgr.getSingleProductColor);
    app.route('/addfilterpriceoptions')
        .post(productSettingMgr.addFilterPriceOptions);
    app.route('/addfilterdiscountoptions')
        .post(productSettingMgr.addFilterDiscountOptions);
    app.route('/addfilterdispatchoptions')
        .post(productSettingMgr.addFilterDispatchOptions);
        app.route('/deletefilterprice/:id/:priceId')
        .delete(productSettingMgr.deleteFilterPrice);
        app.route('/deletefilterdiscount/:id/:discountId')
        .delete(productSettingMgr.deleteFilterDiscount);
        app.route('/deletefilterdispatch/:id/:dispatchId')
        .delete(productSettingMgr.deleteFilterDispatch);
        app.route('/findfilteroptions')
        .get(productSettingMgr.findfilterOptions);
        app.route('/createnote')
        .post(productSettingMgr.createNote);
        app.route('/editnote/:id')
        .put(productSettingMgr.editNote);
        app.route('/deletenote/:id')
        .delete(productSettingMgr.deleteNote);
        app.route('/getnote')
        .get(productSettingMgr.getNote);
        
}
