var CategoryBannerMgr = require('./categoryBannerMgr');

module.exports = function(app) {
    app.route('/createcategorybanner')
    .post(CategoryBannerMgr.createCategoryBanner);              // add Category Banner
  app.route('/addimagename/:id')
    .post(CategoryBannerMgr.addImageName);                      // add Image Name
  app.route('/getcategorybanner')
    .get(CategoryBannerMgr.getCategoryBanner);                         // get All Category Banner
   app.route('/getsinglecategorybanner/:id')
     .get(CategoryBannerMgr.getSingleCategoryBanner);           // get Single Category Banner
  app.route('/updatecategorybanner/:id')
    .put(CategoryBannerMgr.updateCategoryBanner);               // update Category Banner
  app.route('/updateimagename/:id')
    .put(CategoryBannerMgr.updateImageName);                    // update Image Name
  app.route('/deletecategorybanner/:id')
    .delete(CategoryBannerMgr.deleteCategoryBanner);                    // delete Category Banner
}