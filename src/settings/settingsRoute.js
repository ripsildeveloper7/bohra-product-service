'use strict';
var settingsMgr = require('./settingsMgr');
var upload = require('../config/multer.config');
module.exports = function (app) {
    app.route('/productSettings')
        .get(settingsMgr.getProductSettings); // retreive all the details from product settings collection

    app.route('/pricerange')
        .post(settingsMgr.createPriceRange); // create a price range        

    app.route('/removeprice')
        .post(settingsMgr.deletePriceRange); //  delete single prince range

    app.route('/color')
        .post(settingsMgr.createColor); // create a color

    app.route('/removecolor')
        .post(settingsMgr.deleteColor); //remove single color
    app.route('/material')
        .post(settingsMgr.createMaterial); // create a material
    app.route('/removematerial')
        .post(settingsMgr.deleteMaterial); // remove single material 

        app.route('/note')
        .post(settingsMgr.createNote); // create a note
    app.route('/removenote')
        .post(settingsMgr.deleteNote); // remove single note 

    app.route('/occasion')
        .post(settingsMgr.createOccasion); // create an occasion

    app.route('/removeoccasion')
        .post(settingsMgr.deleteOccasion); // remove single occasion

    app.route('/tags')
        .post(settingsMgr.createTags); // create an size

    app.route('/removetags')
        .post(settingsMgr.deleteTags); // remove single size



    // product option  start
    app.route('/createproductoption')
        .post(settingsMgr.createProductOption); // create product options
    app.route('/getproductoption')
        .get(settingsMgr.getProductOption); // get product options
    app.route('/editproductoption/:id')
        .put(settingsMgr.editProductOption); // edit product options
    app.route('/deleteproductoption/:id')
        .delete(settingsMgr.deleteProductOption); // delete product options
    app.route('/getsingleproductoption/:id')
        .get(settingsMgr.getSingleProductOption); // delete product options


    /* app.route('/editproductoption/:id/optionvalue/:optionId')
    .put(settingsMgr.editProductOptionValue);   */
    // product option  end

    // product tag start //
    app.route('/addproducttag') // add product tag
        .post(settingsMgr.addProductTag);
    app.route('/getproducttag') // get product tag
        .get(settingsMgr.getProductTag);
    app.route('/deleteproducttag/:id') // delete product tag
        .delete(settingsMgr.deleteProductTag);
    app.route('/getselecetedproducttag/:id') // get Selected tag
        .get(settingsMgr.getSelectedProductTag);

    app.route('/editproducttag/:id')
        .put(settingsMgr.editProductTag); // edit product tag

    app.route('/getproducttagvalue') // get product tag value
        .get(settingsMgr.findtag);
    app.route('/getfirsttag') // get product tag value
        .get(settingsMgr.firstTag);

    app.route('/getsecondtag') // get product tag value
        .get(settingsMgr.secondTag);

    app.route('/getproductposition/:id') // get product position
        .get(settingsMgr.getProductPosition);

     
        app.route('/getcategoryforproduct/:superId/gettagforproduct/:tagId') // get product by tag and super category id
        .get(settingsMgr.getCategoryTagProduct);    

        app.route('/adddiscountforsetting')
        .post(settingsMgr.createDiscount);     
        app.route('/deletediscountinsetting')
        .post(settingsMgr.deleteDiscount);     

        app.route('/adddispatchtimeforsetting')
        .post(settingsMgr.createDispatchTime);     
        app.route('/deletedispatchtimeinsetting')
        .post(settingsMgr.deleteDispatchTime);    
   
}
