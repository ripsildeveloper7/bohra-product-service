
var categoryRoutes = require('./categories/categoryRoute');
var brandRoutes = require('./brand/brandRoute');
var productRoutes = require('./product/productRoute');
var settingsRoutes = require('./settings/settingsRoute');
var cartRoutes = require('./cart/cartRoute');
var imagesRoutes = require('./images/imagesRoute');
var producFiltertRoutes = require('./productfilter/productFilterRoute');
var productSettingRoutes = require('./productSetting/productSettingRoute');
var productReviewRoutes = require('./productReview/productReviewRoute');
var wishListRoutes = require('./wishList/wishListRoute');
var categoryBannerRoute = require('./categoryBanner/categoryBannerRoute');

exports.loadRoutes = function (app) {
    categoryRoutes(app);
    brandRoutes(app);
    productRoutes(app);
    producFiltertRoutes(app);
    settingsRoutes(app);
    cartRoutes(app);
    imagesRoutes(app);
    productSettingRoutes(app);
    productReviewRoutes(app);
    wishListRoutes(app);
    categoryBannerRoute(app);
};

