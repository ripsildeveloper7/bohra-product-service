'use strict';
var Cart = require('../model/cart.model');

exports.addToCart = function (req, res) {
  Cart.findOne({ userId: req.body.userId }).
    select().exec(function (err, foundCart) {
      if (err) {
        res.status(500).send({
          "result": 0
        });
      } else {
        if (!foundCart) {
          var cart = new Cart();
          cart.userId = req.body.userId;
          cart.items = req.body.items;
          cart.save(function (err, cartDetail) {
            if (err) {
              res.status(500).json(err);
            } else {
              Cart.aggregate([
                { $match: { userId: req.body.userId } },
                { $unwind: "$items" },
                {
                  $lookup:
                  {
                    from: "products",
                    localField: "items.productId",
                    foreignField: "_id",
                    as: "cart_product"
                  }
                }
              ], function (err, cartData) {
                if (err) {
                  res.status(500).send({
                    message: "no cart product"
                  });
                } else {
                  res.status(200).json(cartData);
                }
              });
            }
          });
        } else {
          var itemReq = req.body.items;
          var cartDb = foundCart.items;
          var key = "INTsku";
          var bodyHeight = "bodyHeight";
          var serviceId = "serviceId";
          itemReq.map(element => {
            if (cartDb.find(s => s[key] === element[key])) {
              if (cartDb.find(s => s[key] === element[key] && s[service] === element[service])) {
                if (cartDb.find(s => s[service] === true)) {
                if (cartDb.find(s => s[key] === element[key] && s[service] === element[service]  && s[serviceId] === element[serviceId]  && s[bodyHeight] === element[bodyHeight])) {
                const dbSame = cartDb.find(s => s[key] === element[key] && s[service] === element[service] && s[serviceId] === element[serviceId]  && s[bodyHeight] === element[bodyHeight])
                      dbSame.qty += element.qty  
                } else {
                  foundCart.items.push(element);  
                }
              } else {
                if (cartDb.find(s => s[key] === element[key] && s[service] === element[service]  && s[bodyHeight] === element[bodyHeight])) {
                const dbSame = cartDb.find(s => s[key] === element[key] && s[service] === element[service]  && s[bodyHeight] === element[bodyHeight])
                      dbSame.qty += element.qty
                    } else {
                      foundCart.items.push(element);
                    }
                  } 
              } else {
                foundCart.items.push(element);
              }
            } else {
              foundCart.items.push(element);
            }
          })
          // itemReq.map(element => {
          //   if (cartDb.find(s => s[key] === element[key])) {
          //     if (cartDb.find(s => s[service] === true) && element[service] === true) {
          //       if (cartDb.find(s => s[serviceId] === element[serviceId])) {
          //       if (cartDb.find(s => s[selectedSize] === element[selectedSize])) {
          //         const dbSame = cartDb.find(s => s[key] === element[key] && s[service] === true && s[selectedSize] === element[selectedSize])
          //         dbSame.qty += element.qty
          //       } else {
          //         foundCart.items.push(element);
          //       }
          //     } else {
          //       foundCart.items.push(element);
          //     }
          //     } else if (cartDb.find(s => s[service] !== true) && element[service] === true) {
          //       foundCart.items.push(element);
          //     } else if (cartDb.find(s => s[service] === false) && element[service] === false) {
          //       const dbSame = cartDb.find(s => s[key] === element[key] && s[service] === false)
          //         dbSame.qty += element.qty 
          //     } else if (cartDb.find(s => s[service] !== false) && element[service] === false) {
          //       foundCart.items.push(element);
          //     }
          //     /* else {
          //       if (element[service] === true) {
          //         foundCart.items.push(element);
          //       } else {
          //         const dbSame = cartDb.find(s => s[key] === element[key])
          //         dbSame.qty += element.qty 
          //       }
          //     } */
              
          //   } else {
          //     foundCart.items.push(element);
          //   }
          // });
          foundCart.save(function (err, fountData) {
            if (err) {
              res.status(500).json(err);
            } else {
              Cart.aggregate([
                { $match: { userId: req.body.userId } },
                { $unwind: "$items" },
                {
                  $lookup:
                  {
                    from: "products",
                    localField: "items.productId",
                    foreignField: "_id",
                    as: "cart_product"
                  }
                }
              ], function (err, cartData) {
                if (err) {
                  res.status(500).send({
                    message: "no cart product"
                  });
                } else {
                  res.status(200).json(cartData); 
                }
              });
            }
          });
        }
      }
    });
}

exports.findAllCart = function (req, res) {
  Cart.aggregate([
    { $match: { userId: req.params.userId } },
    { $unwind: "$items" },
    {
      $lookup:
      {
        from: "products",
        localField: "items.productId",
        foreignField: "_id",
        as: "cart_product"
      }
    }
  ], function (err, cartData) {
    if (err) {
      res.status(500).send({
        message: "no cart product"
      });
    } else {
      res.status(200).json(cartData);
    }
  });
}

exports.findCartProductDecrement = function (req, res) {
  Cart.findOne({ userId: req.body.userId }).select().exec(function (err, findProductData) {
    if (err) {
      res.status(500).json(err);
    } else {
      var items = req.body.items;
      for (var i = 0; i < findProductData.items.length; i++) {
        if (findProductData.items[i].sku == items.sku) {
          findProductData.items[i].qty = findProductData.items[i].qty - items.qty;
        }
      }
      findProductData.save(function (err, cartProductDetail) {
        if (err) {
          res.status(500).json(err);
        } else {
          Cart.aggregate([
            { $match: { userId: req.body.userId } },
            { $unwind: "$items" },
            {
              $lookup:
              {
                from: "products",
                localField: "items.productId",
                foreignField: "_id",
                as: "cart_product"
              }
            },
            { $sort : { items : -1 } }
          ], function (err, cartData) {
            if (err) {
              res.status(500).send({
                message: "no cart product"
              });
            } else {
              res.status(200).json(cartData);
            }
          });
        }
      });
    }
  });
}


exports.cartProductDelete = function (req, res) {
  Cart.findOne({ userId: req.params.userId },
    function (err, cartData) {
      if (err) {
        res.status(500).send({
          "result": 0
        });
      } else {
        cartData.items.id(req.params.itemId).remove();
        cartData.save(function (err, cartSave) {
          if (err) {
            res.status(201).send({
              "result": 0
            });
          } else {
            Cart.aggregate([
              { $match: { userId: req.params.userId } },
              { $unwind: "$items" },
              {
                $lookup:
                {
                  from: "products",
                  localField: "items.productId",
                  foreignField: "_id",
                  as: "cart_product"
                }
              }
            ], function (err, cartData) {
              if (err) {
                res.status(500).send({
                  message: "no cart product"
                });
              } else {
                res.status(200).json(cartData);
              }
            });
          }
        });
      }
    });
}


exports.cartDelete = function (req, res) {
  Cart.findOneAndRemove({userId : req.params.userId }, function (err) {
      if (err) {
          res.status(500).send({
              "result": 0
          });
      } else {
        Cart.find({}).select().exec(function (err, deleteCart) {
              if (err) {
                  res.status(500).json({
                      "result": 0
                  })
              } else {
                  res.status(200).json(deleteCart)
              }
          })
      }
  });
}
exports.findAllCartCount = function (req, res) {
  Cart.findOne({ userId: req.params.userId }).select().exec(function (err, cartCount) {
    if (err) {
      res.status(500).json({
        "result": 0
      })
    } else {
      res.status(200).json(cartCount.items.length);
    }
  });
}