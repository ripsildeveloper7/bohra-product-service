var mongoose = require('mongoose');

const FieldAttributeValueSchema = new mongoose.Schema({
    fieldAttributeValue: String
});

module.exports  = FieldAttributeValueSchema;