var mongoose = require('mongoose');
var ProductOptionValue = require('./productOptionValue.model');
const productOptionModelSchema = new mongoose.Schema({
    optionName: String,
    optionValue: [ProductOptionValue]
});

const productOption = mongoose.model('productoption', productOptionModelSchema);
module.exports = productOption;
