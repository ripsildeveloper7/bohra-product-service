var mongoose = require('mongoose');
var FieldAttributeValue = require('./../model/fieldAttributeValue');

const SubCategorySchema = new mongoose.Schema({
    subCategoryName: String,
    subCategoryDescription: String,
    metaTagTitle: String,
    metaTagDescription: String,
    metaTagKeyword: String,
    isDispatch: Boolean,
    subCategoryImageName: String,
    attribute: [{fieldName: String, fieldType: String, sortOrder: Number, fieldSetting: String, fieldEnable: Boolean,
                 fieldEnableValue: Boolean, fieldValue: [FieldAttributeValue] }]
});

module.exports = SubCategorySchema;
