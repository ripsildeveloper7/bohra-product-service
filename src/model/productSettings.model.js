var mongoose = require('mongoose');

const ProductSettingsSchema  = new mongoose.Schema({
    priceRange: [String],
    color: [String],
    material: [String],
    occasion: [String],
    size: [String],
    tags: [String],
    discount: [String],
    dispatchTime: [String],
    note:[String]
});


const ProductSettings = mongoose.model('productsettings', ProductSettingsSchema);
module.exports = ProductSettings;