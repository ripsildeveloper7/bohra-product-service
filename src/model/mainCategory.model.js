var mongoose = require('mongoose');
var SubCategory = require('./subCategory.model');
var FieldAttributeValue = require('./fieldAttributeValue');

const MainCategorySchema = new mongoose.Schema({
    mainCategoryName: String,
    mainCategoryDescription: String,
    mainCategoryImageName: String,
    subCategory: [SubCategory],
    attribute: [{fieldName: String, fieldType: String, fieldSetting: String, fieldEnable: Boolean,
        fieldEnableValue: Boolean, fieldValue: [FieldAttributeValue] }]
});
module.exports = MainCategorySchema;
