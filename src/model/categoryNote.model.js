var mongoose = require('mongoose');

const NoteSchema = new mongoose.Schema({
   superCategoryId: String,
   superCategoryName: String,
   subCategoryId: String,
   subCategoryName: String,
   note: String
});
const categoryNote = mongoose.model('categoryNote', NoteSchema);
module.exports = categoryNote;