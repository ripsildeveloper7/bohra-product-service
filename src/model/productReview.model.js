var mongoose = require('mongoose');

const ProductReviewSchema  = new mongoose.Schema({
    customerName: String,
    customerId: {type: mongoose.Schema.Types.ObjectId, ref: 'ProductSchema' },
    productId: {type: mongoose.Schema.Types.ObjectId, ref: 'ProductSchema' },
    cartId: String,
    review: String,
    createdDate: Date,
    rating: Number,
    customerEmailId: String,
    publish: Boolean,
    country: String,
    orderId: String
});

const productReview = mongoose.model('productreview', ProductReviewSchema);
module.exports = productReview;