var mongoose = require('mongoose');
var productImageName = require('./productImageName.model');
var attributeData = require('./attribute.model');

const ChildSchema = new mongoose.Schema({
  productName: String,
  manufactureInfo: String,
  vendorId: String,
  brandName: String,
  hsnCode: String,
  gstIn: String,
  sku: String,
  styleCode: String,
  variation: String,
  productsType: String,
  variationType: String,
    variationId: String,
    sizeVariantId: String,
    variationTypeId: String,
    colorId: String,
    INTsku: String,
  sizeVariant: String,
  packOf: Number,
  size: String,
  weight: String,
  fabric: String,
  washCare: String,
  productDimension: String,
  packingDimension: String,
  ttsPortol: { type: Number, default: 0 },
  ttsVendor: { type: Number, default: 0 },
  sp: { type: Number, default: 0 },
  vp: { type: Number, default: 0 },
  discount: { type: Number, default: 0 },
  mrp: { type: Number, default: 0 },
  price: { type: Number, default: 0 },
  closure: String,
  pattern: String,
  productDescription: String,
  toFitWaist: String,
  searchTerms1: String,
  searchTerms2: String,
  searchTerms3: String,
  searchTerms4: String,
  searchTerms5: String,
  superCategoryId: {type: mongoose.Schema.Types.ObjectId, ref: 'SuperCategorySchema' },
  superCategoryName: String,
  mainCategoryId: mongoose.Schema.Types.ObjectId,
  mainCategoryName: String,
  subCategoryId: {type: mongoose.Schema.Types.ObjectId, ref: 'SuperCategorySchema' },
  subCategoryName: String,
  color: String,
  colorId: String,
  tags: [{type: mongoose.Schema.Types.ObjectId, ref: 'productTagModelSchema' }],
  metaTitle: String,
  metaDescription: String,
  metaKeyword: String,
  dateAdded:  { type: Date, default: Date.now() },
  dateModified: { type: Date, default: Date.now() },
  brandId: {type: mongoose.Schema.Types.ObjectId, ref: 'BrandSchema' } ,
  sizeVariantId: String,
  publish: Boolean,
  discount: Number,
  sizeName: String,
  sizeId: String,
  sku: String,
  quantity: Number,
  headChild: Boolean,
  productImage: [productImageName],
  catalogueName: String,
  costIncludes: String,
  attribute: [attributeData],
  price: { type: Number, default: 0 },
  tailoringService: String,
  
  /* sku: String, */
  /*  upc: String,
   ean: String,
   jan: String,
   isbn: String,
   mpn: String, */
  
  /* manufacturerId: String, */
  /* productImageName: [String], */
 /*   special: String,
  reward: String, */
  /* bulletPoints: String, */
  /* height: String, */
  /* productId: String, */
  
  /* color: String, */
  
  /* taxId: String,
  rating: Number,
  reviews: String,
  status: Boolean, */
  // category map
  

  // meta tag start
  
 // color
  
  
});

module.exports = ChildSchema;