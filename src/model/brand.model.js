var mongoose = require('mongoose');


const BrandSchema = new mongoose.Schema({
    brandName: String,
    brandTitle: String,
    brandDescription: String,
    brandImageName: String,
    brandContent: String,
    brandStatus: Boolean,
    metaTitle: String,
    metaDescription: String,
    metaContent: String
});


const Brand = mongoose.model('brand', BrandSchema);
module.exports = Brand;
