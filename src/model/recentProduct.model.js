var mongoose = require('mongoose');

const RecentProductSchema = new mongoose.Schema({
    userId: String,
    productId: [{type: mongoose.Schema.Types.ObjectId, ref: 'ProductSchema'}]
});

const recentProduct = mongoose.model('recentproduct', RecentProductSchema);
module.exports = recentProduct;