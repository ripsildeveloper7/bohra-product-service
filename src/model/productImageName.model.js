var mongoose = require('mongoose');
const productImageModelSchema = new mongoose.Schema({
    productImageName: String,
    position: Number
});
module.exports = productImageModelSchema;
