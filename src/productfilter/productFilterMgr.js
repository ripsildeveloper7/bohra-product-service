
var productFilterDA = require('./productFilterDA');
var productLib = require('product-service-lib/src/product/productGet/productGetMgr');


exports.getSearchProductFilter = function (req, res) {
  try {
      productFilterDA.getSearchProductFilter(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getSuperCategoryFilter = function (req, res) {
    try {
        productFilterDA.getSuperCategoryFilter(req, res);
    } catch (error) {
      console.log(error);
    }
  }
  
exports.getMainCategoryFilter = function (req, res) {
  try {
      productFilterDA.getMainCategoryFilter(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getSubCategoryFilter = function (req, res) {
  try {
      productFilterDA.getSubCategoryFilter(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getAttribute = function (req, res) {
  try {
      productFilterDA.getAttribute(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getSupFilterAttributeProduct = function (req, res) {
  try {
      productFilterDA.getSupFilterAttributeProduct(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getAttributeSuperCategoryFilter = function (req, res) {
  try {
      productFilterDA.getAttributeSuperCategoryFilter(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getAttributeSuperCategoryProduct = function (req, res) {
  try {
      productFilterDA.getAttributeSuperCategoryProduct(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getAttributeSubCategoryProduct = function (req, res) {
  try {
      productFilterDA.getAttributeSubCategoryProduct(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.getSupAttribute = function (req, res) {
  try {
      productFilterDA.getSupAttribute(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.bulkChangeType = function (req, res) {
  try {
      productFilterDA.bulkChangeType(req, res);
  } catch (error) {
    console.log(error);
  }
}
/* exports.getAttribute = function (req, res) {
  try {
    productLib.getAttribute(req, res);
  } catch (error) {
    console.log(error);
  }
} */
exports.categoryWiseFilterForAdmin = function (req, res) {
  try {
      productFilterDA.categoryWiseFilterForAdmin(req, res);
  } catch (error) {
    console.log(error);
  }
}
/* exports.priceRangeWiseFilterForAdmin = function (req, res) {
  try {
      productFilterDA.priceRangeWiseFilterForAdmin(req, res);
  } catch (error) {
    console.log(error);
  }
} */
exports.searchProduct = function (req, res) {
  try {
      productFilterDA.searchProduct(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.childSearchProductForAdmin = function (req, res) {
  try {
      productFilterDA.childSearchProductForAdmin(req, res);
  } catch (error) {
    console.log(error);
  }
}