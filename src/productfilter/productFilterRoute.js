'use strict';

var productFilterMgr = require('./productFilterMgr');


module.exports = function (app) {
  app.route('/searchproductfilter')
      .post(productFilterMgr.getSearchProductFilter);
    app.route('/supercatfilter')
      .post(productFilterMgr.getSuperCategoryFilter); // filter product super category
    app.route('/maincatfilter')
      .post(productFilterMgr.getMainCategoryFilter); // filter product main category
    app.route('/subcatfilter')
      .post(productFilterMgr.getSubCategoryFilter); // filter product sub category
    app.route('/supattributeproduct')
      .post(productFilterMgr.getSupFilterAttributeProduct);  // filter product in super 
    app.route('/supercat/:catId/attribute/:attributeId/attributefield/:attributeFieldId')
    .get(productFilterMgr.getAttributeSuperCategoryProduct);
    app.route('/subattribute')
    .post(productFilterMgr.getSupAttribute);
    app.route('/changetest')
    .get(productFilterMgr.bulkChangeType);
 /*    app.route('/attributefields/:catId')
      .get(productFilterMgr.getAttribute); // filter product attribute */
      app.route('/categorywisefilterforadmin')
    .put(productFilterMgr.categoryWiseFilterForAdmin);
   /*  app.route('/pricerangewisefilterforadmin')
    .put(productFilterMgr.priceRangeWiseFilterForAdmin); */
    app.route('/searchproductforadmin')
    .put(productFilterMgr.searchProduct);
    app.route('/searchchildproductforadmin')
    .put(productFilterMgr.childSearchProductForAdmin);
  }