var Product = require('../model/product.model');
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
exports.getSuperCategoryFilter = function (req, res) {
 /*  var conditionColor = {};
  if (req.body.color.length > 0) {
    conditionColor["color"] = {
      $in: req.body.color
    };
  }
  var conditionAttribute = {}
  if (req.body.attribute.length > 0) {
    conditionAttribute["child.attribute"]= {
      $text: { $search: req.body.attribute}
    }; 
  }
  var conditionSize = {};
  if (req.body.size.length > 0) {
    conditionSize["child.sizeVariant"] = {
      $in: req.body.size
    };
  }
  var conditionPrice = {};
  Product.find({
    $and: [{
        superCategoryId: req.body.superCategoryId
      }, conditionColor,
        conditionSize, conditionPrice,
        conditionAttribute
    ]
  }).select().exec(function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(data);
    }
  }); */
  
  
  var conditionSize = {};
  var conditionColor = {};
  var conditionAttribute = {};
  var conditionPrice = {};
  var conditionDiscount = {};
  var conditionDispatch = {};
  // if(req.body.pageNo === undefined){
  //   var perPage = 30;
  //   var pageNo = 1;
  // }else{
  //    pageNo = parseInt(req.body.pageNo);
  //    perPage = parseInt(req.body.perPage);
  // }
  // var paginatorDetails = {}
  // if(pageNo < 0 || pageNo === 0) {
  //       data = {"error" : true,"message" : "invalid page number, should start with 1"};
  //       return res.json(data)
  // }
  // paginatorDetails.skip = perPage * (pageNo - 1);
  // paginatorDetails.limit = perPage;

  var sizeData = req.body.attribute.find(function (detail) {
     
     return detail.fieldName === 'Size' ; 
  });
  var priceMaxData = req.body.attribute.find(function (detail) {
     
    return detail.fieldName === 'maxPrice'; 
 });
 var priceMinData = req.body.attribute.find(function (detail) {
     
  return detail.fieldName === 'minPrice'; 
});
var discountMaxData = req.body.attribute.find(function (detail) {
     
  return detail.fieldName === 'maxDiscount'; 
});
var discountMinData = req.body.attribute.find(function (detail) {
   
return detail.fieldName === 'discount'; 
});
var dispatData = req.body.attribute.find(function (detail) {
   
  return detail.fieldName === 'dispatch'; 
  });
  var colorData = req.body.attribute.find(function (detail) {
     
    return detail.fieldName === 'Color'; 
 });
 var paginatorPage = req.body.attribute.find(function (detail) {
     
  return detail.fieldName === 'pageNo'; 
});

 if(priceMaxData && priceMinData ){
  conditionPrice["price"] = {
  $gte: parseInt(priceMinData.fieldValue[0]),
  $lte:  parseInt(priceMaxData.fieldValue[0])
};
}
if(discountMinData){
  conditionDiscount["discount"] = {
    $gte:   parseInt(discountMinData.fieldValue[0])
  }
 
}
if(dispatData){
  conditionDispatch["ttsPortol"] ={  
   $lte: parseInt(dispatData.fieldValue[0])
  }
};
  if (sizeData) {
    conditionSize["child.sizeVariant"] = {
      $in: sizeData.fieldValue
    };
  }
  if (colorData) {
    conditionColor["child.color"] = {
      $in: colorData.fieldValue
    };
  }
  
  if(!paginatorPage){
    var perPage = 30;
    var pageNo = 1;
   } else{
     pageNo = parseInt(paginatorPage.fieldValue[0]);
     perPage = parseInt(30);
   
  }
  var attributeData = req.body.attribute.filter(function (detail) {
     
    return (detail.fieldName !== 'Size') && (detail.fieldName !== 'Color')  && (detail.fieldName !== 'minPrice') && (detail.fieldName !== 'maxPrice') && (detail.fieldName !== 'maxDiscount') && (detail.fieldName !== 'discount') && (detail.fieldName !== 'dispatch')&& (detail.fieldName !== 'pageNo'); 
 });
 
  if (attributeData.length > 0) {
  var key, keys = [];
  for (let i = 0; i < attributeData.length; i++) {
    if(attributeData[i].fieldValue.length > 0) {
      /* element = { ['child.attribute']: { $elemMatch: {[req.body.attribute[i]['fieldName']]: {$in: req.body.attribute[i].fieldValue}}}} */
      element = { ['child.attribute']: { $elemMatch: {[attributeData[i]['fieldName']]: {$in: attributeData[i].fieldValue}}}}
      keys.push(element);
    }
  } 
  var conditionAttribute = { $and: keys}  
}

// Product.countDocuments({
//   $and: [{
//     superCategoryId: req.body.superCategoryId,
//     publish: true
//     },
   
//     conditionSize,
//     conditionColor,
//     conditionAttribute,
//     conditionPrice,
//     conditionDiscount,
//     conditionDispatch]}).exec(function (err, totalData) {
   
//   if(err) {
//     data = {"error" : true,"message" : "Error fetching data"}
//   }
//   var paginatorDetails = {};
//   if(pageNo < 0 || pageNo === 0) {
//     data = {"error" : true,"message" : "invalid page number, should start with 1"};
//     return res.json(data)
//   }
//   paginatorDetails.skip = perPage * (pageNo - 1);
//   paginatorDetails.limit = perPage;
//   Product.find({
//     $and: [{
//       superCategoryId: req.body.superCategoryId,
//       publish: true
//       },
     
//       conditionSize,
//       conditionColor,
//       conditionAttribute,
//       conditionPrice,
//       conditionDiscount,
//       conditionDispatch,

//     ]
//   },{}, paginatorDetails,function (err, data) {
//     if (err) {
//       res.status(500).send({
//         message: "Some error occurred while retrieving notes."
//       });
//     } else {
//       var totalpages = Math.ceil(totalData/perPage); 
//       data ={"product":data,"pages":totalpages};
//       res.status(200).json(data);
//     }
//   });
//  })
// }

var skip =  pageNo > 0 ? ( ( pageNo - 1 ) * perPage ) : 0 ;
 Product.aggregate([
  {'$match':  { $and:[{ superCategoryId: ObjectId(req.body.superCategoryId)},{publish: true}]}},
	{'$match':{ $and :[ conditionSize,
  conditionColor,
  conditionAttribute,
  conditionSize,
  conditionPrice,
  conditionDiscount,
  conditionDispatch] }},

  // { '$group': {
  //   _id: {
  //     superCategoryId: '$superCategoryId',
  //     productName: '$productName',
  //      price: '$price',
  //       sp: '$sp',
  //        productImage: '$productImage',
  //         discount: '$discount',
  //         child: '$child.quantity',
  //         _id: '$_id'
  //   }
  // }},
  {
    $project: {
      superCategoryId: 1,
      productName: 1,
      price: 1,
      sp: 1,
      productImage: 1,
      discount: 1,
      'child.quantity': 1,
      seqOrder: 1,
      description: { $ifNull: ["$seqOrder", Infinity]}
    }
  },
  {
    $sort: {
     description: 1,
     seqOrder: -1
    }
  },

  { '$facet'    : {
      metadata: [ { $count: "total" }, { $addFields: { page: pageNo } } ],
      data: [ { $skip: skip }, { $limit: perPage } ] 
  } }
]).exec(function (err, data) {

  if (err) {
    res.status(500).send({
      message: "Some error occurred while retrieving notes."
    });
  } else {
    res.status(200).json(data);
  }
});
}


//   Product.find({
//     $and: [{
//       superCategoryId: req.body.superCategoryId
//       },
//       conditionSize,
//       conditionColor,
//       conditionAttribute,
//       conditionPrice,
//       conditionDiscount,
//       conditionDispatch
//     /*   {
//         "child.attribute":{$all: { $and: [ { 
//           "fabricType" : "A"
//     }, {
//       "workType" : "Trational"
//     }
//       ]
//     }
//   }
//   } */  /*  conditionAttribute */
//   /* { $and: [
//          {'child.attribute': {'$elemMatch': { "workType" : "Trational"} }},
//          {'child.attribute': {'$elemMatch': {   "fabricType" : "A"} } }
//     ]
//     } */
//     ]
//   }).select().exec(function (err, data) {
//     if (err) {
//       res.status(500).send({
//         message: "Some error occurred while retrieving notes."
//       });
//     } else {
//       console.log(data);
//       res.status(200).json(data);
//     }
//   });
// }

  exports.getMainCategoryFilter = function (req, res) {
  var conditionColor = {};
  var conditionPrice = {};
  if(req.body.maxPrice !== null){
    conditionPrice["price"] = {
    $gte: req.body.minPrice,
    $lte: req.body.maxPrice
  };
}
  var conditionAttribute = {}
  if (req.body.attribute.length > 0) {
    conditionAttribute["child.attribute"]= {
      $text: { $search: req.body.attribute}
    };
  }
  if (req.body.color.length > 0) {
    conditionColor["color"] = {
      $in: req.body.color
    };
  }
  var conditionSize = {};
  if (req.body.size.length > 0) {
    conditionSize["child.sizeVariant"] = {
      $in: req.body.size
    };
  }
  var paginatorPage = req.body.attribute.find(function (detail) {
     
    return detail.fieldName === 'pageNo'; 
  });
  
  var skip =  pageNo > 0 ? ( ( pageNo - 1 ) * perPage ) : 0 ;
 Product.aggregate([
  {'$match':  { $and:[{ mainCategoryId: ObjectId(req.body.mainCategoryId)},{publish: true}]}},
	{'$match':{ $and :[ conditionSize,
  conditionColor,
  // conditionAttribute,
  // conditionSize,
  conditionPrice,
  // conditionDiscount,
  // conditionDispatch
] }},

  // { '$group': {
  //   _id: {
  //     subCategoryId: '$subCategoryId',
  //     productName: '$productName',
  //      price: '$price',
  //       sp: '$sp',
  //        productImage: '$productImage',
  //         discount: '$discount',
  //         // child: '$child.quantity',
  //         _id: '$_id'
  //   }
  // }},
  {
    $project: {
      mainCategoryId: 1,
      productName: 1,
      price: 1,
      sp: 1,
      productImage: 1,
      discount: 1,
      'child.quantity': 1,
      seqOrder: 1,
      description: { $ifNull: ["$seqOrder", Infinity]}
    }
  },
  {
    $sort: {
     description: 1,
     seqOrder: -1
    }
  },
  { '$facet'    : {
      metadata: [ { $count: "total" }, { $addFields: { page: pageNo } } ],
      data: [ { $skip: skip }, { $limit: perPage } ] 
  } }
]).exec(function (err, data) {

  if (err) {
    res.status(500).send({
      message: "Some error occurred while retrieving notes."
    });
  } else {
    res.status(200).json(data);
  }
});
}

//   Product.find({
//     $and: [{
//       mainCategoryId: req.body.mainCategoryId,
//       publish: true
//       }, conditionColor,
//         conditionSize,
//         conditionPrice,
//         conditionAttribute
//     ]
//   }).select().exec(function (err, data) {
//     if (err) {
//       res.status(500).send({
//         message: "Some error occurred while retrieving notes."
//       });
//     } else {
//       res.status(200).json(data);
//     }
//   });
// }

exports.getSubCategoryFilter = function (req, res) {
  /* var conditionColor = {};
  var conditionPrice = {};
  if(req.body.maxPrice !== null){
    conditionPrice["price"] = {
    $gte: req.body.minPrice,
    $lte: req.body.maxPrice
  };
}
  if (req.body.color.length > 0) {
    conditionColor["color"] = {
      $in: req.body.color
    };
  }
  var testAttribute = { $and: [
    {'child.attribute': {'$elemMatch': { "workType" : "Trational"} }},
    {'child.attribute': {'$elemMatch': {  "fabricCare" : "Velvet"} } }
]
}
  var conditionAttribute = {};
  if (req.body.attribute.length > 0) {
  var key, keys = [];
  for (let i = 0; i < req.body.attribute.length; i++) {
    if(req.body.attribute[i].fieldValue.length > 0) {
      element = { ['child.attribute']: { $elemMatch: {[req.body.attribute[i]['fieldName']]: {$in: req.body.attribute[i].fieldValue}}}}
      keys.push(element);
    }
  }
}
var conditionAttribute = { $and: keys}
  if (req.body.attribute.length > 0) {
    for(let i = 0; i < req.body.attribute.length; i++) {
      if (!testAttribute[req.body.attribute[i].fieldName]) {
        console.log(conditionAttribute["child.attribute"] = {})
      }
    }
    var output = Object.entries(testAttribute).map(([key, value]) => {
      obj = {
        key,
        value
      }
      conditionAttribute.push(obj);
    });

    console.log(testAttribute);
    conditionAttribute["child.attribute"] = { $elemMatch: testAttribute};
console.log(conditionAttribute);
  var conditionSize = {};
  if (req.body.size.length > 0) {
    conditionSize["child.sizeVariant"] = {
      $in: req.body.size
    };
  } */
  
  var conditionSize = {};
  var conditionColor = {};
  var conditionAttribute = {};
  var conditionPrice = {};
  var conditionDiscount = {};
  var conditionDispatch = {};

  var sizeData = req.body.attribute.find(function (detail) {
     
     return detail.fieldName === 'Size'; 
  });
  var priceMaxData = req.body.attribute.find(function (detail) {
     
    return detail.fieldName === 'maxPrice'; 
 });
 var priceMinData = req.body.attribute.find(function (detail) {
     
  return detail.fieldName === 'minPrice'; 
});
var discountMaxData = req.body.attribute.find(function (detail) {
     
  return detail.fieldName === 'maxDiscount'; 
});
var discountMinData = req.body.attribute.find(function (detail) {
   
return detail.fieldName === 'discount'; 
});
var dispatData = req.body.attribute.find(function (detail) {
   
  return detail.fieldName === 'dispatch'; 
  });
  var colorData = req.body.attribute.find(function (detail) {
     
    return detail.fieldName === 'Color'; 
 });
 var paginatorPage = req.body.attribute.find(function (detail) {
     
  return detail.fieldName === 'pageNo'; 
});

 if(priceMaxData && priceMinData ){
  conditionPrice["price"] = {
  $gte: parseInt(priceMinData.fieldValue[0]),
  $lte: parseInt(priceMaxData.fieldValue[0])
};
}
if(discountMinData){
  conditionDiscount["discount"] = {
    $gte: parseInt(discountMinData.fieldValue[0])
  }
}
if(dispatData){
  conditionDispatch["ttsPortol"] ={  
   $lte: parseInt(dispatData.fieldValue[0])
  }
};
  if (sizeData) {
    conditionSize["child.sizeVariant"] = {
      $in: sizeData.fieldValue
    };
  }
  if(!paginatorPage){
    var perPage = 30;
    var pageNo = 1;
   } else{
     pageNo = parseInt(paginatorPage.fieldValue[0]);
     perPage = parseInt(30);
   
  }
  if (colorData) {
    conditionSize["child.color"] = {
      $in: colorData.fieldValue
    };
  }
  var attributeData = req.body.attribute.filter(function (detail) {
     
    return (detail.fieldName !== 'Size') && (detail.fieldName !== 'Color')  && (detail.fieldName !== 'minPrice') && (detail.fieldName !== 'maxPrice') && (detail.fieldName !== 'maxDiscount') && (detail.fieldName !== 'discount') && (detail.fieldName !== 'dispatch')&& (detail.fieldName !== 'pageNo'); 
 });
  if (attributeData.length > 0) {
  var key, keys = [];
  for (let i = 0; i < attributeData.length; i++) {
    if(attributeData[i].fieldValue.length > 0) {
      /* element = { ['child.attribute']: { $elemMatch: {[req.body.attribute[i]['fieldName']]: {$in: req.body.attribute[i].fieldValue}}}} */
      element = { ['child.attribute']: { $elemMatch: {[attributeData[i]['fieldName']]: {$in: attributeData[i].fieldValue}}}}
      keys.push(element);
    }
  } 
  var conditionAttribute = { $and: keys}  
}
var skip =  pageNo > 0 ? ( ( pageNo - 1 ) * perPage ) : 0 ;
 Product.aggregate([
  {'$match':  { $and:[{ subCategoryId: ObjectId(req.body.subCategoryId)},{publish: true}]}},
	{'$match':{ $and :[ conditionSize,
  conditionColor,
  conditionAttribute,
  conditionSize,
  conditionPrice,
  conditionDiscount,
  conditionDispatch] }},

  // { '$group': {
  //   _id: {
  //     subCategoryId: '$subCategoryId',
  //     productName: '$productName',
  //      price: '$price',
  //       sp: '$sp',
  //        productImage: '$productImage',
  //         discount: '$discount',
  //         // child: '$child.quantity',
  //         _id: '$_id'
  //   }
  // }},
  {
    $project: {
      subCategoryId: 1,
      productName: 1,
      price: 1,
      sp: 1,
      productImage: 1,
      discount: 1,
      'child.quantity': 1,
      seqOrder: 1,
      description: { $ifNull: ["$seqOrder", Infinity]}
    }
  },
  {
    $sort: {
     description: 1,
     seqOrder: -1
    }
  },
  { '$facet'    : {
      metadata: [ { $count: "total" }, { $addFields: { page: pageNo } } ],
      data: [ { $skip: skip }, { $limit: perPage } ] 
  } }
]).exec(function (err, data) {

  if (err) {
    res.status(500).send({
      message: "Some error occurred while retrieving notes."
    });
  } else {
    res.status(200).json(data);
  }
});
}





//   Product.find({
//     $and: [{
//       subCategoryId: req.body.subCategoryId,
//       publish: true
//       },
//       conditionSize,
//       conditionColor,
//       conditionAttribute,
//       conditionPrice,
//       conditionDiscount,
//       conditionDispatch
//      /*  conditionAttribute,
//       {
//         "child.attribute":{$all: { $and: [ { 
//           "fabricType" : "A"
//     }, {
//       "workType" : "Trational"
//     }
//       ]
//     }
//   }
//   },   conditionAttribute,
//   { $and: [
//          {'child.attribute': {'$elemMatch': { "workType" : "Trational"} }},
//          {'child.attribute': {'$elemMatch': {   "fabricType" : "A"} } }
//     ]
//     } */
//     ]
//   }).select().exec(function (err, data) {
//     if (err) {
//       res.status(500).send({
//         message: "Some error occurred while retrieving notes."
//       });
//     } else {
//       console.log(data);
//       res.status(200).json(data);
//     }
//   });
  
// }




exports.getSearchProductFilter = function (req, res) {
 
  var paginatorPage = req.body.attribute.find(function (detail) {
      
   return detail.fieldName === 'pageNo'; 
 });
 
  
   
   if(!paginatorPage){
     var perPage = 30;
     var pageNo = 1;
    } else{
      pageNo = parseInt(paginatorPage.fieldValue[0]);
      perPage = parseInt(30);
    
   }
   var attributeData = req.body.attribute.filter(function (detail) {
      
     return  (detail.fieldName !== 'pageNo'); 
  });
  
//    if (attributeData.length > 0) {
//    var key, keys = [];
//    for (let i = 0; i < attributeData.length; i++) {
//      if(attributeData[i].fieldValue.length > 0) {
//        /* element = { ['child.attribute']: { $elemMatch: {[req.body.attribute[i]['fieldName']]: {$in: req.body.attribute[i].fieldValue}}}} */
//        element = { ['child.attribute']: { $elemMatch: {[attributeData[i]['fieldName']]: {$in: attributeData[i].fieldValue}}}}
//        keys.push(element);
//      }
//    } 
//    var conditionAttribute = { $and: keys}  
//  }

 
 var skip =  pageNo > 0 ? ( ( pageNo - 1 ) * perPage ) : 0 ;
  Product.aggregate([
    { '$match'    : { $text: {
      $search: req.body.searchId
    }  } },


  //  {'$match':  { $and:[ { $text: {
  //   $search: req.body.searchId
  // } }]}},
  //  {'$match':{ $and :[ conditionSize,
  //  conditionColor,
  //  conditionAttribute,
  //  conditionSize,
  //  conditionPrice,
  //  conditionDiscount,
  //  conditionDispatch] }},
 
   // { '$group': {
   //   _id: {
   //     superCategoryId: '$superCategoryId',
   //     productName: '$productName',
   //      price: '$price',
   //       sp: '$sp',
   //        productImage: '$productImage',
   //         discount: '$discount',
   //         child: '$child.quantity',
   //         _id: '$_id'
   //   }
   // }},
   {
     $project: {
       superCategoryId: 1,
       mainCategoryId:1,
       subCategoryId:1,
       productName: 1,
       price: 1,
       sp: 1,
       productImage: 1,
       discount: 1,
       'child.quantity': 1,
       seqOrder: 1,
       description: { $ifNull: ["$seqOrder", Infinity]}
     }
   },
   {
     $sort: {
      description: 1,
      seqOrder: -1
     }
   },
 
   { '$facet'    : {
       metadata: [ { $count: "total" }, { $addFields: { page: pageNo } } ],
       data: [ { $skip: skip }, { $limit: perPage } ] 
   } }
 ]).exec(function (err, data) {
 
   if (err) {
     res.status(500).send({
       message: "Some error occurred while retrieving notes."
     });
   } else {
     res.status(200).json(data);
   }
 });
 }


exports.getAttribute = function (req, res) {
  Product.distinct("child.attribute", { superCategoryId: req.params.catId }).select().exec(function (err, items) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      var array = []
      const merged = items.reduce((accum, obj) => {
        for (key in obj) {
          accum[key] = accum[key] ? [...accum[key], obj[key]] : [obj[key]]; 
           
        }
        return accum;
      }, { });
      for (var key in merged) {
          if (merged.hasOwnProperty(key)) {
              array.push({"type" : key, "name" : merged[key] })
          }
      }
      res.status(200).send(array);
    }
  });
}
/* 

exports.getFilterAttribute = function (req, res) {
  Product.distinct("child.attribute", { superCategoryId: req.params.catId }).select().exec(function (err, items) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      var array = []
      const merged = items.reduce((accum, obj) => {
        for (key in obj) {
          accum[key] = accum[key] ? [...accum[key], obj[key]] : [obj[key]]; 
           
        }
        return accum;
      }, { });
      for (var key in merged) {
          if (merged.hasOwnProperty(key)) {
              array.push({"type" : key, "name" : merged[key] })
          }
      }
      res.status(200).send(array);
    }
  });
} */


exports.getSupFilterAttributeProduct = function (req, res) {
  
  var conditionSize = {};
  var conditionColor = {};
  var conditionAttribute = {};
  var conditionPrice = {};
  var conditionDiscount = {};
  var conditionDispatch = {};

  var sizeData = req.body.attribute.find(function (detail) {
     
     return detail.fieldName === 'Size'; 
  });
  var priceMaxData = req.body.attribute.find(function (detail) {
     
    return detail.fieldName === 'maxPrice'; 
 });
 var priceMinData = req.body.attribute.find(function (detail) {
     
  return detail.fieldName === 'minPrice'; 
});
var discountMaxData = req.body.attribute.find(function (detail) {
     
  return detail.fieldName === 'maxDiscount'; 
});
var discountMinData = req.body.attribute.find(function (detail) {
   
return detail.fieldName === 'discount'; 
});
var dispatData = req.body.attribute.find(function (detail) {
   
  return detail.fieldName === 'dispatch'; 
  });
  var colorData = req.body.attribute.find(function (detail) {
     
    return detail.fieldName === 'Color'; 
 });
 var paginatorPage = req.body.attribute.find(function (detail) {
     
  return detail.fieldName === 'pageNo'; 
});
 if(priceMaxData && priceMinData ){
  conditionPrice["price"] = {
  $gte: parseInt(priceMinData.fieldValue[0]),
  $lte: parseInt(priceMaxData.fieldValue[0])
};
}
if(discountMinData){
  conditionDiscount["discount"] = {
    $gte: parseInt(discountMinData.fieldValue[0])
  }
}
if(dispatData){
  conditionDispatch["ttsPortol"] ={  
   $in: parseInt(dispatData.fieldValue)
  }
};
  if (sizeData) {
    conditionSize["child.sizeVariant"] = {
      $in: sizeData.fieldValue
    };
  }
  if (colorData) {
    conditionSize["child.color"] = {
      $in: colorData.fieldValue
    };
  }
  if(!paginatorPage){
    var perPage = 30;
    var pageNo = 1;
   } else{
     pageNo = parseInt(paginatorPage.fieldValue[0]);
     perPage = parseInt(30);
   
  }
  var attributeData = req.body.attribute.filter(function (detail) {
     
    return (detail.fieldName !== 'Size') && (detail.fieldName !== 'Color')  && (detail.fieldName !== 'minPrice') && (detail.fieldName !== 'maxPrice') && (detail.fieldName !== 'maxDiscount') && (detail.fieldName !== 'discount') && (detail.fieldName !== 'dispatch')&& (detail.fieldName !== 'pageNo'); 
 });
  if (attributeData.length > 0) {
  var key, keys = [];
  for (let i = 0; i < attributeData.length; i++) {
    if(attributeData[i].fieldValue.length > 0) {
      /* element = { ['child.attribute']: { $elemMatch: {[req.body.attribute[i]['fieldName']]: {$in: req.body.attribute[i].fieldValue}}}} */
      element = { ['child.attribute']: { $elemMatch: {[attributeData[i]['fieldName']]: {$in: attributeData[i].fieldValue}}}}
      keys.push(element);
    }
  } 
  var conditionAttribute = { $and: keys}  
}
var skip =  pageNo > 0 ? ( ( pageNo - 1 ) * perPage ) : 0 ;
 Product.aggregate([
  {'$match':  { $and:[{ subCategoryId: ObjectId(req.body.subCategoryId)},{publish: true},
  {'child.attribute': { $elemMatch: {attributeId: req.params.attributeId}}},
  {'child.attribute': { $elemMatch: {attributeFieldId: req.params.attributeFieldId}} }]}},
	{'$match':{ $and :[ conditionSize,
  conditionColor,
  conditionAttribute,
  conditionSize,
  conditionPrice,
  conditionDiscount,
  conditionDispatch] }},

  { '$group': {
    _id: {
      subCategoryId: '$subCategoryId',
      productName: '$productName',
       price: '$price',
        sp: '$sp',
         productImage: '$productImage',
          discount: '$discount',
          // child: '$child.quantity',
          _id: '$_id'
    }
  }},

  { '$facet'    : {
      metadata: [ { $count: "total" }, { $addFields: { page: pageNo } } ],
      data: [ { $skip: skip }, { $limit: perPage } ] 
  } }
]).exec(function (err, data) {

  if (err) {
    res.status(500).send({
      message: "Some error occurred while retrieving notes."
    });
  } else {
    res.status(200).json(data);
  }
});
}



//   Product.find({ $and: [
//     { superCategoryId: req.params.catId,
//       publish: true },
//     {'child.attribute': { $elemMatch: {attributeId: req.params.attributeId}}},
//     {'child.attribute': { $elemMatch: {attributeFieldId: req.params.attributeFieldId}} }
// ],  conditionSize,
//     conditionColor,
//     conditionAttribute,
//     conditionPrice,
//     conditionDiscount,
//     conditionDispatch}).select().exec(function (err, items) {
//     if (err) {
//       res.status(500).send({
//         message: "Some error occurred while retrieving notes."
//       });
//     } else {
//       res.status(200).send(items);
//     }
//   });
// } 
exports.getAttributeSuperCategoryProduct = function (req, res) {
  Product.find({ $and: [
    { superCategoryId: req.params.catId,
      publish: true },
    {'child.attribute': { $elemMatch: {attributeId: req.params.attributeId}}},
    {'child.attribute': { $elemMatch: {attributeFieldId: req.params.attributeFieldId}} }
]}).select().exec(function (err, items) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).send(items);
    }
  });
} 

exports.getAttributeSubCategoryProduct = function (req, res) {
  Product.find({ $and: [
    { subCategoryId: req.params.catId,
      publish: true },
    {'child.attribute': { $elemMatch: {attributeId: req.params.attributeId}}},
    {'child.attribute': { $elemMatch: {attributeFieldId: req.params.attributeFieldId}} }
]}).select().exec(function (err, products) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).send(products);
    }
  });
}

exports.getSupAttribute = function (req, res) {
  Product.find({ $and: [
    { subCategoryId: req.params.catId,
      publish: true },
    {'child.attribute': { $elemMatch: {attributeId: req.params.attributeId}}},
    {'child.attribute': { $elemMatch: {attributeFieldId: req.params.attributeFieldId}} }
]}).select().exec(function (err, products) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).send(products);
    }
  });
}

exports.bulkChangeType = function (req, res) {
  Product.find({ "ttsPortol": { "$exists": true, "$type": 2 }}).then(users=>{ 
    users.forEach(el => {
      console.log(el.ttsPortol);
    if(el.ttsPortol){
      Product.update({"_id":el._id}, {"$set":  {
       "ttsPortal": {$type : 1 }}
     });
     console.log(el);
    }
/*   */
  });
  });
}
// admin filter
exports.categoryWiseFilterForAdmin = function(req, res) {
  const pageNo = parseInt(req.body.pageNo);
  const perPage = parseInt(req.body.perPage);
  const skip = pageNo > 0 ? ( ( pageNo - 1 ) * perPage ) : 0 ;
  Product.aggregate([
    { '$match'    : { $or: [
    { superCategoryId: ObjectId(req.body.superCategoryId) },
    { price: {$gte: req.body.minPrice, $lte: req.body.maxPrice} },
    { vendorId: req.body.vendorId }
    ]} },
    { '$facet'    : {
        metadata: [ { $count: "total" }, { $addFields: { page: pageNo } } ],
        data: [ { $skip: skip }, { $limit: perPage } ] // add projection here wish you re-shape the docs
    } }
] ).exec(function(err, data) {
  if (err) {
    res.status(500).json(err);
  } else {
    res.status(200).json(data);
  }
})
}
exports.searchProduct = function(req, res) {
  const pageNo = parseInt(req.body.pageNo);
  const perPage = parseInt(req.body.perPage);
  const skip = pageNo > 0 ? ( ( pageNo - 1 ) * perPage ) : 0 ;
  Product.aggregate([
    { '$match'    : { $or: [
      { sku: { '$regex': req.body.search, $options: 'g' } },
      { productName: { '$regex': req.body.search, $options: 'g' } }
      ]} },
    { '$facet'    : {
      metadata: [ { $count: "total" }, { $addFields: { page: pageNo } } ],
      data: [ { $skip: skip }, { $limit: perPage } ] // add projection here wish you re-shape the docs
  } }
  ]).exec(function(err, data) {
  if (err) {
    res.status(500).json(err);
  } else {
    res.status(200).json(data);
  }
})
}
/* exports.searchProduct = function(req, res) {
  var pageNo = parseInt(req.body.pageNo);
  var perPage = parseInt(req.body.perPage);
  var products = {}
  if(pageNo < 0 || pageNo === 0) {
        data = {"error" : true,"message" : "invalid page number, should start with 1"};
        return res.json(data)
  }
  products.skip = perPage * (pageNo - 1)
  products.limit = perPage
  
  Product.count({ $or : [{"sku" :  { '$regex': "^" + req.body.search}},
  {'productName': { '$regex': '^' + req.body.search}}]},function(err,totalCount) {
             if(err) {
               data = {"error" : true,"message" : "Error fetching data"}
             }
             Product.find({},{},products,function(err,data) {
            
            if(err) {
              res.status(500).json(error);
            } else {
                var totalPages = Math.ceil(totalCount / perPage)
                data = {"error" : false,"products" : data,"pages": totalPages};
            }
           res.status(200).json(data);
         });
       })
} */
exports.childSearchProductForAdmin = function(req, res) {
  const pageNo = parseInt(req.body.pageNo);
  const perPage = parseInt(req.body.perPage);
  const skip = pageNo > 0 ? ( ( pageNo - 1 ) * perPage ) : 0 ;
  Product.aggregate([
    {
      $match: {
       $or: [
          {child: {$elemMatch: {'styleCode': { '$regex': req.body.search }}} },
          {child: {$elemMatch: {'productName': { '$regex': req.body.search }}} }
        ]
      }
    },
    {
      $unwind:  '$child'
    },
    {
      $match: {
        $or: [
          {
            'child.productName': { '$regex': req.body.search , $options: 'g' }
          },
          {
            'child.styleCode': { '$regex': req.body.search  , $options: 'g'} 
          }
        ]
      }
    },
    { '$facet'    : {
      metadata: [ { $count: "total" }, { $addFields: { page: pageNo } } ],
      data: [ { $skip: skip }, { $limit: perPage } ] // add projection here wish you re-shape the docs
  } }
  ]).exec(function(err, data) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  })
}

/* exports.priceRangeWiseFilterForAdmin = function(req, res) {
  const pageNo = parseInt(req.body.pageNo);
  const perPage = parseInt(req.body.perPage);
  const skip = pageNo > 0 ? ( ( pageNo - 1 ) * perPage ) : 0 ;
  Product.aggregate([
    { '$match'    : { price: {$gte: req.body.minPrice, $lte: req.body.maxPrice}} },
    { '$facet'    : {
        metadata: [ { $count: "total" }, { $addFields: { page: pageNo } } ],
        data: [ { $skip: skip }, { $limit: perPage } ] // add projection here wish you re-shape the docs
    } }
] ).exec(function(err, data) {
  if (err) {
    res.status(500).json(err);
  } else {
    res.status(200).json(data);
  }
})
} */
