const MongoClient = require('mongodb').MongoClient
 
// Note: A production application should not expose database credentials in plain text.
// For strategies on handling credentials, visit 12factor: https://12factor.net/config.
const PROD_URI = "mongodb://localhost:27017/bohra-product";
const MKTG_URI = "mongodb+srv://bohraadmin:admin@cluster0-u2ulm.mongodb.net/test?retryWrites=true&w=majority"
 
function connect(url) {
  return MongoClient.connect(url, { useNewUrlParser: true }).then(client => client.db());
}
 
module.exports = async function() {
  try {
    let databases = await Promise.all([connect(MKTG_URI)])
    return {
      production: databases[0]
    }
  } catch (err) {
      console.log(err);
  }
}