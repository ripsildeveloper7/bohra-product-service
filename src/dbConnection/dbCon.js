var dbCon;

var updateConnection = function (con) {
    dbCon = con;
}


module.exports = {
    updateConnection: updateConnection,
    dbCon: dbCon
}