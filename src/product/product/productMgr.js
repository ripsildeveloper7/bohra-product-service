var productDA = require('./productDA');
var IDgenerator = require('./../../model/IDgenerator.model');
var s3 = require('./../../config/s3.config');
var env = require('./../../config/s3.env');
var productLib = require('product-service-lib/src/product/productGet/productGetMgr');

exports.addProduct = function (req, res) {
  try {
    productDA.addProduct(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.uploadProduct = function (req, res) {
  try {
    IDgenerator.find({}).select({}).exec(function (err, IDgeneratorCheck) {
      if (err) {
        res.status(500).send({
          message: "Some error occurred while retrieving notes."
        });
      } else {
        if(IDgeneratorCheck.length === 0) {
          IDgenerator = new IDgenerator();
          IDgenerator.save(function (err, productData) {
            if (err) { // if it contains error return 0
              res.status(500).send({
                "result": 'error not save id'
              });
            } else {
              productDA.uploadProduct(req, productData.INTsku, productData.PINTsku,  res);
            }
          });
        } else {
          
          productDA.uploadProduct(req, IDgeneratorCheck[0].INTsku, IDgeneratorCheck[0].PINTsku,  res);
        }
      }
    });
  } catch (error) {
    console.log(error);
  }
}
exports.updateBulkUpload = function (req, res) {
  try {
    IDgenerator.find({}).select({}).exec(function (err, IDgeneratorCheck) {
      if (err) {
        res.status(500).send({
          message: "Some error occurred while retrieving notes."
        });
      } else {
        if(IDgeneratorCheck.length === 0) {
          IDgenerator = new IDgenerator();
          IDgenerator.save(function (err, productData) {
            if (err) { // if it contains error return 0
              res.status(500).send({
                "result": 'error not save id'
              });
            } else {
              productDA.updateBulkUpload(req, productData.INTsku, res);
            }
          });
        } else {
          productDA.updateBulkUpload(req, IDgeneratorCheck[0].INTsku, res);
        }
      }
    });
  
  } catch (error) {
    console.log(error);
  }
}


exports.getProduct = function (req, res) {
  try {
    productDA.getProduct(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getAllProductForAdmin = function (req, res) {
  try {
    productDA.getAllProductForAdmin(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.parentProudctImageUpdate = function (req, res) {
  try {
    productDA.parentProudctImageUpdate(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.childProudctImageUpdate = function (req, res) {
  try {
    productDA.childProudctImageUpdate(req, res);
  } catch (error) {
    console.log(error);
  }
}
/* exports.getProductForUI = function (req, res) {
  try {
    productDA.getProductForUI(req, res);
  } catch (error) {
    console.log(error);
  }
} */

exports.getChildProduct = function (req, res) {
  try {
    productDA.getChildProduct(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getAttributeProduct = function (req, res) {
  try {
    productDA.getAttributeProduct(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.getProductByDate = function (req, res) {
  try {
    productDA.getProductByDate(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.addSizeVariant = function (req, res) {
  try {
    productDA.addSizeVariant(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.editProductVariant = function (req, res) {
  try {
    productDA.editProductVariant(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.editProductCategory = function (req, res) {
  try {
    productDA.editProductCategory(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.editProductBrand = function (req, res) {
  try {
    productDA.editProductBrand(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.deleteProduct = function (req, res) {
  try {
    productDA.deleteProduct(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.deleteProductVariant = function (req, res) {
  try {
    productDA.deleteProductVariant(req, res);
  } catch (error) {
    console.log(error);
  }
}


exports.editProductInfo = function (req, res) {
  try {
    productDA.editProductInfo(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.editProductSeo = function (req, res) {
  try {
    productDA.editProductSeo(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getSingleProduct = function (req, res) {
  try {
    productDA.getSingleProduct(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getProductPriceRange = function (req, res) {
  try {
    productDA.getProductPriceRange(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.deleteSingleProductImage = function (req, res) {
  try {
    productDA.deleteSingleProductImage(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.sortProductByDate = function (req, res) {
  try {
    productDA.sortProductByDate(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.sortProductByName = function (req, res) {
  try {
    productDA.sortProductByName(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.productOverallCount = function (req, res) {
  try {
    productDA.productOverallCount(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getSingleProductWithBrand = function (req, res) {
  try {
    productDA.getSingleProductWithBrand(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.updatePublishProduct = function (req, res) {
  try {
    productDA.updatePublishProduct(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.UpdateUnPublishProduct = function (req, res) {
  try {
    productDA.UpdateUnPublishProduct(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getAllProductMainCategory = function (req, res) {
  try {
    productDA.getAllProductMainCategory(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getAllProductSuperCategory = function (req, res) {
  try {
    productDA.getAllProductSuperCategory(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getAllProductSubCategory = function (req, res) {
  try {
    productDA.getAllProductSubCategory(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getProductForRow = function (req, res) {
  try {
    productDA.getProductForRow(req, res);
  } catch (error) {
    console.log(error);
  }
}


exports.getProductByIdDemo = function (req, res) {    // Demo
  try {
    productDA.getProductByIdDemo(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getProductChild = function(req, res) {
  try {
    productDA.getProductChild(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getPromotionProduct = function (req, res) {
  try {
    productDA.getPromotionProduct(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.searchProduct = function (req, res) {
  try {
    productDA.searchProduct(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getAllProductBrand = function (req, res) {
  try {
    productDA.getAllProductBrand(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.youMayAlsoLike = function (req, res) {
  try {
    productDA.youMayAlsoLike(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.bulkWriteExample = function (req, res) {
  try {
    productDA.bulkWriteExample(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.multiDeleteProduct = function (req, res) {
  try {
    productDA.multiDeleteProduct(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.pagination = function (req, res) {
  try {
    productDA.pagination(req, res);
  } catch (error) {
    console.log(error);
  }
}

exports.getSupercategoryAndFilter = function (req, res) {
  try {
    productDA.getSupercategoryAndFilter(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getSubcategoryAndFilter = function (req, res) {
  try {
    productDA.getSubcategoryAndFilter(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getProductBySuperCategoryForSequence = function (req, res) {
  try {
    productDA.getProductBySuperCategoryForSequence(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getProductBySubCategoryForSequence = function (req, res) {
  try {
    productDA.getProductBySubCategoryForSequence(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.addSuperCategorySequenceOrder = function (req, res) {
  try {
    productDA.addSuperCategorySequenceOrder(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.addSubCategorySequenceOrder = function (req, res) {
  try {
    productDA.addSubCategorySequenceOrder(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getInventoryQty = function (req, res) {
  try {
    productDA.getInventoryQty(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getCategoryWiseProductCount = function (req, res) {
  try {
    productDA.getCategoryWiseProductCount(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getOutOfStockQtyCount = function (req, res) {
  try {
    productDA.getOutOfStockQtyCount(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getProductBySuperCategory = function (req, res) {
  try {
    productDA.getProductBySuperCategory(req, res);
  } catch (error) {
    console.log(error);
  }
}
exports.getProductBySubCategory = function (req, res) {
  try {
    productDA.getProductBySubCategory(req, res);
  } catch (error) {
    console.log(error);
  }
}