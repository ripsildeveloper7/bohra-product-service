var Product = require('./../../model/product.model');
var Child = require('./../../model/child.model');
var mongoose = require('mongoose');
var s3 = require('./../../config/s3.config');
var env = require('./../../config/s3.env');
var IDgenerator = require('./../../model/IDgenerator.model');
var ObjectId = mongoose.Types.ObjectId;
var SuperCategory = require('../../model/superCategory.model');
var FilterOption = require('../../model/filter-option.model');

exports.addProduct = function (req, res) {
  var product = new Product();
  product.productName = req.body.productName;
  product.manufactureInfo = req.body.manufactureInfo;
  product.brandName = req.body.brandName;
  product.hsnCode = req.body.hsnCode;
  product.gstIn = req.body.gstIn;
  product.styleCode = req.body.styleCode;
  product.variation = req.body.variation;
  product.productsType = req.body.productsType;
  product.packOf = req.body.packOf;
  product.size = req.body.size;
  product.price = req.body.price;
  product.weight = req.body.weight;
  product.fabric = req.body.fabric;
  product.washCare = req.body.washCare;
  product.productDimension = req.body.productDimension;
  product.packingDimension = req.body.packingDimension;
  product.discount = req.body.discount;
  product.sp = req.body.sp;
  product.closure = req.body.closure;
  product.pattern = req.body.pattern;
  product.productDescription = req.body.productDescription;
  product.toFitWaist = req.body.toFitWaist;
  /* 
    product.superCategoryId = req.body.superCategoryId;
    product.mainCategoryId = req.body.mainCategoryId;
    product.subCategoryId = req.body.subCategoryId; */
  product.metaTitle = req.body.metaTitle;
  product.metaDescription = req.body.metaDescription;
  product.productVariantType = req.body.productVariantType;
  product.metaKeyword = req.body.metaKeyword;
  product.brandId = req.body.brandId;
  product.brandName = req.body.brandName;
  product.child = req.body.child;
  product.sizeVariantId = req.body.sizeVariantId;
  product.sizeVariantId = req.body.productImage;
  /* product.productVariant = req.body.productVariant; */
  product.discount = req.body.discount;
  product.dateAdded = Date.now();
  product.save(function (err, productData) {
    if (err) { // if it contains error return 0
      res.status(500).send({
        "result": 'error occured while retreiving data'
      });
    } else {
      res.status(200).json(productData);
    }
  });

}
exports.uploadProduct = function (req, currentINTsku, currentPINTsku, res) {
  const saveData = [];
  const data = req.body;
  console.log(currentINTsku, 'test');
  const oldINTsku = parseInt(currentINTsku);
  const oldPINTsku = parseInt(currentPINTsku);
  data.map((pro) => {
    var product = new Product();
    const childData = [];
    parentINTskuConverter = ++currentPINTsku;
    product.productName = pro.productName,
      product.manufactureInfo = pro.manufactureInfo,
      product.vendorId = pro.vendorId,
      product.brandName = pro.brandName,
      product.hsnCode = pro.hsnCode,
      product.gstIn = pro.gstIn,
      product.sku = pro.sku,
      product.styleCode = pro.styleCode,
      product.variation = pro.variation,
      product.productsType = pro.productsType,
      product.variationType = pro.variationType,
      product.variationId = pro.variationId,
      product.sizeVariantId = pro.sizeVariantId,
      product.variationTypeId = pro.variationTypeId,
      product.colorId = pro.colorId,
      product.sizeVariant = pro.sizeVariant,
      product.packOf = pro.packOf,
      product.size = pro.size,
      product.weight = pro.weight,
      product.fabric = pro.fabric,
      product.washCare = pro.washCare,
      product.productDimension = pro.productDimension,
      product.packingDimension = pro.packingDimension,
      product.ttsPortol = pro.ttsPortol,
      product.ttsVendor = pro.ttsVendor,
      product.sp = pro.sp,
      product.vp = pro.vp,
      product.discount = pro.discount,
      product.mrp = pro.mrp,
      product.price = pro.price,
      product.closure = pro.closure,
      product.pattern = pro.pattern,
      product.productDescription = pro.productDescription,
      product.toFitWaist = pro.productDescription,
      product.searchTerms1 = pro.searchTerms1,
      product.searchTerms2 = pro.searchTerms2,
      product.searchTerms3 = pro.searchTerms3,
      product.searchTerms4 = pro.searchTerms4,
      product.searchTerms5 = pro.searchTerms5,
      product.superCategoryId = pro.superCategoryId,
      product.superCategoryName = pro.superCategoryName,
      product.mainCategoryId = pro.mainCategoryId,
      product.mainCategoryName = pro.mainCategoryName,
      product.subCategoryId = pro.subCategoryId,
      product.subCategoryName = pro.subCategoryName,
      product.color = pro.color,
      product.colorId = pro.colorId,
      product.metaTitle = pro.metaTitle,
      product.metaDescription = pro.metaDescription,
      product.metaKeyword = pro.metaKeyword,
      product.dateAdded = Date.now(),
      product.dateModified = Date.now(),
      product.brandId = pro.brandId,
      product.sizeVariantId = pro.sizeVariantId,
      product.publish = true,
      product.discount = pro.discount,
      product.sizeName = pro.sizeName,
      product.sizeId = pro.sizeId,
      product.quantity = pro.quantity,
      product.productImage = pro.productImage,
      product.catalogueName = pro.catalogueName,
      product.costIncludes = pro.costIncludes,
      product.attribute = pro.attribute,
      product.price = pro.price,
      product.PINTsku = parentINTskuConverter.toString();
      pro.child.forEach(function (childSku) {
      if (childSku) {
        var child = new Product();
        var INTskuConver = ++currentINTsku;
        child.productName = childSku.productName,
        child.manufactureInfo = childSku.manufactureInfo,
        child.vendorId = childSku.vendorId,
        child.brandName = childSku.brandName,
        child.hsnCode = childSku.hsnCode,
        child.gstIn = childSku.gstIn,
        child.sku = childSku.sku,
        child.styleCode = childSku.styleCode,
        child.variation = childSku.variation,
        child.productsType = childSku.productsType,
        child.variationType = childSku.variationType,
        child.variationId = childSku.variationId,
        child.sizeVariantId = childSku.sizeVariantId,
        child.variationTypeId = childSku.variationTypeId,
        child.colorId = childSku.colorId,
        child.sizeVariant = childSku.sizeVariant,
        
        child.INTsku =  INTskuConver.toString()/* ++currentINTsku; */
        child.packOf = childSku.packOf,
        child.size = childSku.size,
        child.weight = childSku.weight,
        child.fabric = childSku.fabric,
        child.washCare = childSku.washCare,
        child.productDimension = childSku.productDimension,
        child.packingDimension = childSku.packingDimension,
        child.ttsPortol = childSku.ttsPortol,
        child.ttsVendor = childSku.ttsVendor,
        child.sp = childSku.sp,
        child.vp = childSku.vp,
        child.discount = childSku.discount,
        child.mrp = childSku.mrp,
        child.price = childSku.price,
        child.closure = childSku.closure,
        child.pattern = childSku.pattern,
        child.productDescription = childSku.productDescription,
        child.toFitWaist = childSku.productDescription,
        child.searchTerms1 = childSku.searchTerms1,
        child.searchTerms2 = childSku.searchTerms2,
        child.searchTerms3 = childSku.searchTerms3,
        child.searchTerms4 = childSku.searchTerms4,
        child.searchTerms5 = childSku.searchTerms5,
        child.superCategoryId = childSku.superCategoryId,
        child.superCategoryName = childSku.superCategoryName,
        child.mainCategoryId = childSku.mainCategoryId,
        child.mainCategoryName = childSku.mainCategoryName,
        child.subCategoryId = childSku.subCategoryId,
        child.subCategoryName = childSku.subCategoryName,
        child.color = childSku.color,
        child.colorId = childSku.colorId,
        child.metaTitle = childSku.metaTitle,
        child.metaDescription = childSku.metaDescription,
        child.metaKeyword = childSku.metaKeyword,
        child.dateAdded = Date.now(),
        child.dateModified = Date.now(),
        child.brandId = childSku.brandId,
        child.sizeVariantId = childSku.sizeVariantId,
        child.publish = childSku.publish,
        child.discount = childSku.discount,
        child.sizeName = childSku.sizeName,
        child.sizeId = childSku.sizeId,
        child.quantity = childSku.quantity,
        child.headChild = childSku.headChild,
        child.productImage = childSku.productImage,
        child.catalogueName = childSku.catalogueName,
        child.costIncludes = childSku.costIncludes,
        child.attribute = childSku.attribute,
        child.price = childSku.price,
        childData.push(child);
      }
  });
  product.child = childData;
  saveData.push(product);
})
  Product.insertMany(saveData, function (err, productData) {
    if (err) { // if it contains error return 0
      res.status(500).send({
        "result": 'error occured while retreiving data'
      });
    } else {
      IDgenerator.updateOne({ INTsku: oldINTsku.toString() }, { INTsku: currentINTsku.toString(), PINTsku: currentPINTsku.toString() }, function (err, IDgeneratorupdate) {
        if (err) {
          res.status(500).send({
            "result": 'error occured while retreiving data'
          });
        } else {
          res.status(200).json(productData);
        }
      });
    }
  });
}
exports.updateBulkUpload = function(req, oldINTsku, res) {
  const temp = req.body;
  const array = [];
  for (const a of temp) {
    array.push(a.sku);
  }
  Product.find({sku: array}).select().exec(function(err, product) {
    if (err) {
      res.status(500).json(err);
    } else {
      const currentINTsku = parseInt(oldINTsku);
      const newProduct = req.body;
      let newChild = false;
      for (const old of product) {
        for (const ne of newProduct) {
          if (old.sku === ne.sku && old.vendorId === ne.vendorId) {
            old.productName = ne.productName,
            old.manufactureInfo = ne.manufactureInfo,
            old.vendorId = ne.vendorId,
            old.brandName = ne.brandName,
            old.hsnCode = ne.hsnCode,
            old.gstIn = ne.gstIn,
            old.sku = ne.sku,
            old.styleCode = ne.styleCode,
            old.variation = ne.variation,
            old.productsType = ne.productsType,
            old.variationType = ne.variationType,
            old.variationId = ne.variationId,
            old.sizeVariantId = ne.sizeVariantId,
            old.variationTypeId = ne.variationTypeId,
            old.colorId = ne.colorId,
            old.sizeVariant = ne.sizeVariant,
            old.packOf = ne.packOf,
            old.size = ne.size,
            old.weight = ne.weight,
            old.fabric = ne.fabric,
            old.washCare = ne.washCare,
            old.productDimension = ne.productDimension,
            old.packingDimension = ne.packingDimension,
            old.ttsPortol = ne.ttsPortol,
            old.ttsVendor = ne.ttsVendor,
            old.sp = ne.sp,
            old.vp = ne.vp,
            old.discount = ne.discount,
            old.mrp = ne.mrp,
            old.price = ne.price,
            old.closure = ne.closure,
            old.pattern = ne.pattern,
            old.productDescription = ne.productDescription,
            old.toFitWaist = ne.productDescription,
            old.searchTerms1 = ne.searchTerms1,
            old.searchTerms2 = ne.searchTerms2,
            old.searchTerms3 = ne.searchTerms3,
            old.searchTerms4 = ne.searchTerms4,
            old.searchTerms5 = ne.searchTerms5,
            old.superCategoryId = ne.superCategoryId,
            old.superCategoryName = ne.superCategoryName,
            old.mainCategoryId = ne.mainCategoryId,
            old.mainCategoryName = ne.mainCategoryName,
            old.subCategoryId = ne.subCategoryId,
            old.subCategoryName = ne.subCategoryName,
            old.color = ne.color,
            old.colorId = ne.colorId,
            old.metaTitle = ne.metaTitle,
            old.metaDescription = ne.metaDescription,
            old.metaKeyword = ne.metaKeyword,
            old.dateModified = Date.now(),
            old.brandId = ne.brandId,
            old.sizeVariantId = ne.sizeVariantId,
            old.publish = true,
            old.discount = ne.discount,
            old.sizeName = ne.sizeName,
            old.sizeId = ne.sizeId,
            old.quantity = ne.quantity,
            old.productImage = ne.productImage,
            old.catalogueName = ne.catalogueName,
            old.costIncludes = ne.costIncludes,
            old.attribute = ne.attribute,
            old.price = ne.price,
            old.child.forEach(oldChild => {
              ne.child.forEach(newChild => {
                if (oldChild.sku === newChild.sku) {
              oldChild.productName = newChild.productName,
              oldChild.manufactureInfo = newChild.manufactureInfo,
              oldChild.vendorId = newChild.vendorId,
              oldChild.brandName = newChild.brandName,
              oldChild.hsnCode = newChild.hsnCode,
              oldChild.gstIn = newChild.gstIn,
  /*             oldChild.sku = newChild.sku, */
              oldChild.styleCode = newChild.styleCode,
              oldChild.variation = newChild.variation,
              oldChild.productsType = newChild.productsType,
              oldChild.variationType = newChild.variationType,
              oldChild.variationId = newChild.variationId,
              oldChild.sizeVariantId = newChild.sizeVariantId,
              oldChild.variationTypeId = newChild.variationTypeId,
              oldChild.colorId = newChild.colorId,
              oldChild.sizeVariant = newChild.sizeVariant,
              oldChild.packOf = newChild.packOf,
              oldChild.size = newChild.size,
              oldChild.weight = newChild.weight,
              oldChild.fabric = newChild.fabric,
              oldChild.washCare = newChild.washCare,
              oldChild.productDimension = newChild.productDimension,
              oldChild.packingDimension = newChild.packingDimension,
              oldChild.ttsPortol = newChild.ttsPortol,
              oldChild.ttsVendor = newChild.ttsVendor,
              oldChild.sp = newChild.sp,
              oldChild.vp = newChild.vp,
              oldChild.discount = newChild.discount,
              oldChild.mrp = newChild.mrp,
              oldChild.price = newChild.price,
              oldChild.closure = newChild.closure,
              oldChild.pattern = newChild.pattern,
              oldChild.productDescription = newChild.productDescription,
              oldChild.toFitWaist = newChild.productDescription,
              oldChild.searchTerms1 = newChild.searchTerms1,
              oldChild.searchTerms2 = newChild.searchTerms2,
              oldChild.searchTerms3 = newChild.searchTerms3,
              oldChild.searchTerms4 = newChild.searchTerms4,
              oldChild.searchTerms5 = newChild.searchTerms5,
              oldChild.superCategoryId = newChild.superCategoryId,
              oldChild.superCategoryName = newChild.superCategoryName,
              oldChild.mainCategoryId = newChild.mainCategoryId,
              oldChild.mainCategoryName = newChild.mainCategoryName,
              oldChild.subCategoryId = newChild.subCategoryId,
              oldChild.subCategoryName = newChild.subCategoryName,
              oldChild.color = newChild.color,
              oldChild.colorId = newChild.colorId,
              oldChild.metaTitle = newChild.metaTitle,
              oldChild.metaDescription = newChild.metaDescription,
              oldChild.metaKeyword = newChild.metaKeyword,
              oldChild.dateModified = Date.now(),
              oldChild.brandId = newChild.brandId,
              oldChild.sizeVariantId = newChild.sizeVariantId,
              oldChild.publish = newChild.publish,
              oldChild.discount = newChild.discount,
              oldChild.sizeName = newChild.sizeName,
              oldChild.sizeId = newChild.sizeId,
              oldChild.quantity = newChild.quantity,
              oldChild.headChild = newChild.headChild,
              oldChild.productImage = newChild.productImage,
              oldChild.catalogueName = newChild.catalogueName,
              oldChild.costIncludes = newChild.costIncludes,
              oldChild.attribute = newChild.attribute,
              oldChild.price = newChild.price
            } else {
              if (newChild.sku) {
                newChild = true;
                const child = new Product();
                var INTskuConver = ++currentINTsku;
        child.productName = newChild.productName,
        child.manufactureInfo = newChild.manufactureInfo,
        child.vendorId = newChild.vendorId,
        child.brandName = newChild.brandName,
        child.hsnCode = newChild.hsnCode,
        child.gstIn = newChild.gstIn,
        child.sku = newChild.sku,
        child.styleCode = newChild.styleCode,
        child.variation = newChild.variation,
        child.productsType = newChild.productsType,
        child.variationType = newChild.variationType,
        child.variationId = newChild.variationId,
        child.sizeVariantId = newChild.sizeVariantId,
        child.variationTypeId = newChild.variationTypeId,
        child.colorId = newChild.colorId,
        child.sizeVariant = newChild.sizeVariant,
        
        child.INTsku =  INTskuConver.toString()/* ++currentINTsku; */
        child.packOf = newChild.packOf,
        child.size = newChild.size,
        child.weight = newChild.weight,
        child.fabric = newChild.fabric,
        child.washCare = newChild.washCare,
        child.productDimension = newChild.productDimension,
        child.packingDimension = newChild.packingDimension,
        child.ttsPortol = newChild.ttsPortol,
        child.ttsVendor = newChild.ttsVendor,
        child.sp = newChild.sp,
        child.vp = newChild.vp,
        child.discount = newChild.discount,
        child.mrp = newChild.mrp,
        child.price = newChild.price,
        child.closure = newChild.closure,
        child.pattern = newChild.pattern,
        child.productDescription = newChild.productDescription,
        child.toFitWaist = newChild.productDescription,
        child.searchTerms1 = newChild.searchTerms1,
        child.searchTerms2 = newChild.searchTerms2,
        child.searchTerms3 = newChild.searchTerms3,
        child.searchTerms4 = newChild.searchTerms4,
        child.searchTerms5 = newChild.searchTerms5,
        child.superCategoryId = newChild.superCategoryId,
        child.superCategoryName = newChild.superCategoryName,
        child.mainCategoryId = newChild.mainCategoryId,
        child.mainCategoryName = newChild.mainCategoryName,
        child.subCategoryId = newChild.subCategoryId,
        child.subCategoryName = newChild.subCategoryName,
        child.color = newChild.color,
        child.colorId = newChild.colorId,
        child.metaTitle = newChild.metaTitle,
        child.metaDescription = newChild.metaDescription,
        child.metaKeyword = newChild.metaKeyword,
        child.dateAdded = Date.now(),
        child.dateModified = Date.now(),
        child.brandId = newChild.brandId,
        child.sizeVariantId = newChild.sizeVariantId,
        child.publish = newChild.publish,
        child.discount = newChild.discount,
        child.sizeName = newChild.sizeName,
        child.sizeId = newChild.sizeId,
        child.quantity = newChild.quantity,
        child.headChild = newChild.headChild,
        child.productImage = newChild.productImage,
        child.catalogueName = newChild.catalogueName,
        child.costIncludes = newChild.costIncludes,
        child.attribute = newChild.attribute,
        child.price = newChild.price,
        ne.child.push(child);
              }
            }
            })
            })
          }
        }
      }
      saveAll();
      function saveAll( callback ){
        var count = 0;
        product.forEach(function(doc){
            doc.save(function(err, saveData){
              if (err) {
                res.status(500).json(err);
              } else {
                count++;
                if( count == product.length ){
                  if (newChild === true) {
                    IDgenerator.updateOne({ INTsku: oldINTsku.toString() }, { INTsku: currentINTsku.toString(), PINTsku: currentPINTsku.toString() }, function (err, IDgeneratorupdate) {
                      if (err) {
                        res.status(500).send({
                          "result": 'error occured while retreiving data'
                        });
                      } else {
                        res.status(200).json(saveData);
                      }
                    });
                  }  else {
                    res.status(200).json(saveData);
                  }
                  }
              }
            });
        });
      }
    }
  })
  /* var batch = Product.collection.initializeOrderedBulkOp({useLegacyOps: true});
  batch.find({'vendorId': req.body.vendorId,
              'sku': req.body.sku}).update({$set: {productName: req.body.productName}});
              batch.execute(function(err, data) {
                if (err) {
                  res.status(500).json(err);
                } else {
                  res.status(200).json(data);
                }
              }) */
}
exports.parentProudctImageUpdate = function(req, res) {
  Product.findOne({'_id': req.params.id}).select().exec(function(err, findData) {
    if (err) {
      res.status(500).json(err);
    } else {
      findData.productImage = req.body.productImage;
      findData.save(function(err, updateData) {
        if (err) {
          res.status(500).json(err);
        } else {
          res.status(200).json(updateData);
        }
      })
    }
  })
}
exports.childProudctImageUpdate = function(req, res) {
  Product.findOne({'_id': req.params.id}).select().exec(function(err, findData) {
    if (err) {
      res.status(500).json(err);
    } else {
      const childData = findData.child.id(req.params.childId);
      childData.productImage = req.body.productImage;
      findData.save(function(err, updateData) {
        if (err) {
          res.status(500).json(err);
        } else {
          res.status(200).json(updateData);
        }
      })
    }
  })
}
/* exports.addSizeVariant = function (req, res) {
  const sizeValue = req.body.size;
  ProductVariant.insertMany(sizeValue, function (err, productSizeData) {
    if (err) { // if it contains error return 0
      res.status(500).send({
        "result": 'error occured while retreiving data'
      });
    } else {
      Product.aggregate([{
        $lookup: {
          from: "productvariants",
          localField: "_id",
          foreignField: "productId",
          as: "productdetails"
        },
      },
      {
        $match: {
          _id: ObjectId(req.body.id)
        }
      },
    ], function (err, allProductData) {
      if (err) {
        res.status(500).send({
          message: "no product"
        });
      } else {
        res.status(200).json(allProductData[0]);
      }
    });
    }
  });
} */
exports.addSizeVariant = function (req, res) {
  const sizeValue = req.body.size;
  Product.findOneAndUpdate({
    _id: req.params.id
  }, {
    $addToSet: {
      productVariant: {
        $each: sizeValue
      }
    }
  }).select().exec(function (err, productSizeData) {
    if (err) { // if it contains error return 0
      res.status(500).send({
        "result": 'error occured while retreiving data'
      });
    } else {
      Product.findOne({
        _id: req.params.id
      }).select().exec(function (err, data) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          res.status(200).json(data);
        }
      });
    }
  });
}
exports.getProduct = function (req, res) {
  Product.find({}).select().exec(function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(data);
    }
  });
}
exports.getAllProductForAdmin = function (req, res) {
    var pageNo = parseInt(req.body.pageNo);
    var perPage = parseInt(req.body.perPage);
    var products = {}
    if(pageNo < 0 || pageNo === 0) {
          data = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(data)
    }
    products.skip = perPage * (pageNo - 1)
    products.limit = perPage
    
    Product.count({},function(err,totalCount) {
               if(err) {
                 data = {"error" : true,"message" : "Error fetching data"}
               }
               Product.find({},{},products,function(err,data) {
              
              if(err) {
                res.status(500).json(error);
              } else {
                  var totalPages = Math.ceil(totalCount / perPage)
                  data = {"error" : false,"products" : data,"pages": totalPages};
              }
             res.status(200).json(data);
           });
         })
}
/* exports.getProductForUI = function (req, res) {
  Product.find({'publish': true}).select().exec(function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(data);
    }
  });
} */

exports.getChildProduct = function (req, res) {
  Product.aggregate([
    { $unwind: '$child' }
  ]).exec(function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(data);
    }
  });
}
exports.getProductByDate = function (req, res) {
  Product.find({}).sort({
    'dateAdded': -1
  }).exec(function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(data);
    }
  });
}
exports.getSingleProduct = function (req, res) {
  Product.findOne({
    _id: req.params.id
  }).select().exec(function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(data);
    }
  });
}
exports.editProductInfo = function (req, res) {
  Product.findOne({
    _id: req.params.id
  }).select().exec(function (err, productEdit) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      productEdit.productName = req.body.productName;
      productEdit.productDescription = req.body.productDescription;
      productEdit.manufacturer = req.body.manufacturer;
      productEdit.bulletPoints = req.body.bulletPoints;
      productEdit.height = req.body.height;
      productEdit.weight = req.body.weight;
      productEdit.material = req.body.material;
      productEdit.occassion = req.body.occassion;
      productEdit.color = req.body.color;
      productEdit.tags = req.body.tags;
      productEdit.price = req.body.price;
      productEdit.quantity = req.body.quantity;
      productEdit.dateModified = Date.now();
      productEdit.save(function (err, product) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          Product.findOne({
            _id: req.params.id
          }).select().exec(function (err, data) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(data);
            }
          });
          /*  Product.aggregate([{
               $lookup: {
                 from: "productvariants",
                 localField: "_id",
                 foreignField: "productId",
                 as: "productdetails"
               },
             },
             {
               $match: {
                 _id: ObjectId(req.params.id)
               }
             },
           ], function (err, allProductData) {
             if (err) {
               res.status(500).send({
                 message: "no product"
               });
             } else {
               res.status(200).json(allProductData[0]);
             }
           }); */
        }
      });
    }
  });
}
exports.deleteProductVariant = function (req, res) {
  Product.findOne({
    _id: req.params.id
  }).select().exec(function (err, product) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      product.child.id(req.params.productVariantId).remove();
      product.save(function (err, product) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          /*  Product.aggregate([{
               $lookup: {
                 from: "productvariants",
                 localField: "_id",
                 foreignField: "productId",
                 as: "productdetails"
               },
             },
             {
               $match: {
                 _id: ObjectId(product.productId)
               }
             },
           ], function (err, allProductData) {
             if (err) {
               res.status(500).send({
                 message: "no product"
               });
             } else {
               res.status(200).json(allProductData[0]);
             }
           }); */
          Product.find({}).select().exec(function (err, data) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(data);
            }
          });
        }
      });
    }
  });
}
exports.editProductSeo = function (req, res) {
  Product.findOne({
    _id: req.params.id
  }).select().exec(function (err, productEdit) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      productEdit.metaTitle = req.body.metaTitle;
      productEdit.metaDescription = req.body.metaDescription;
      productEdit.metaKeyword = req.body.metaKeyword;
      productEdit.dateModified = Date.now();
      productEdit.save(function (err, product) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          Product.findOne({
            _id: req.params.id
          }).select().exec(function (err, data) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(data);
            }
          });
          /*   Product.aggregate([{
                $lookup: {
                  from: "productvariants",
                  localField: "_id",
                  foreignField: "productId",
                  as: "productdetails"
                },
              },
              {
                $match: {
                  _id: ObjectId(req.params.id)
                }
              },
            ], function (err, allProductData) {
              if (err) {
                res.status(500).send({
                  message: "no product"
                });
              } else {
                res.status(200).json(allProductData[0]);
              }
            }); */
        }
      });
    }
  });
}
exports.editProductCategory = function (req, res) {
  Product.findOne({
    _id: req.params.id
  }).select().exec(function (err, productEdit) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      productEdit.superCategoryId = req.body.superCategoryId;
      productEdit.mainCategoryId = req.body.mainCategoryId;
      productEdit.subCategoryId = req.body.subCategoryId;
      productEdit.dateModified = Date.now();
      productEdit.save(function (err, product) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          /*  Product.aggregate([{
               $lookup: {
                 from: "productvariants",
                 localField: "_id",
                 foreignField: "productId",
                 as: "productdetails"
               },
             },
             {
               $match: {
                 _id: ObjectId(req.params.id)
               }
             },
           ], function (err, allProductData) {
             if (err) {
               res.status(500).send({
                 message: "no product"
               });
             } else {
               res.status(200).json(allProductData[0]);
             }
           }); */
          Product.findOne({
            _id: req.params.id
          }).select().exec(function (err, data) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(data);
            }
          });
        }
      });
    }
  });
}
exports.editProductBrand = function (req, res) {
  Product.findOne({
    _id: req.params.id
  }).select().exec(function (err, productEdit) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      productEdit.brandId = req.body.brandId;
      productEdit.save(function (err, product) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          Product.findOne({
            _id: req.params.id
          }).select().exec(function (err, data) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(data);
            }
          });
          /*  Product.aggregate([{
               $lookup: {
                 from: "productvariants",
                 localField: "_id",
                 foreignField: "productId",
                 as: "productdetails"
               },
             },
             {
               $match: {
                 _id: ObjectId(req.params.id)
               }
             },
           ], function (err, allProductData) {
             if (err) {
               res.status(500).send({
                 message: "no product"
               });
             } else {
               res.status(200).json(allProductData[0]);
             }
           }); */
        }
      });
    }
  });
}
/* 
exports.addProductImage = function (req, file, res) {
  Product.findOne({
    '_id': req.params.id
  }, function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      data.productImageName = file.map(e => e.originalname);
      data.dateModified = Date.now();
      data.save(function (err, productSave) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          res.status(200).json(productSave);
        }
      });
    }
  });
} */
exports.editProductVariant = function (req, res) {
  Product.findOne({
    _id: req.params.id
  }).select().exec(function (err, product) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      var productVariantEdit = product.productVariant.id(req.params.productVariantId);
      productVariantEdit.sizeName = req.body.sizeName;
      productVariantEdit.sizeQuantity = req.body.sizeQuantity;
      productVariantEdit.sizePrice = req.body.sizePrice;
      product.save(function (err, product) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          /*  Product.aggregate([{
               $lookup: {
                 from: "productvariants",
                 localField: "_id",
                 foreignField: "productId",
                 as: "productdetails"
               },
             },
             {
               $match: {
                 _id: ObjectId(product.productId)
               }
             },
           ], function (err, allProductData) {
             if (err) {
               res.status(500).send({
                 message: "no product"
               });
             } else {
               res.status(200).json(allProductData[0]);
             }
           }); */
          Product.findOne({
            _id: req.params.id
          }).select().exec(function (err, data) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(data);
            }
          });
        }
      });
    }
  });
}
/* 
exports.getProduct = function (req, res) {
  Product.aggregate([{
    $lookup: {
      from: "productvariants",
      localField: "_id",
      foreignField: "productId",
      as: "productdetails"
    }
  }], function (err, allProductData) {
    if (err) {
      res.status(500).send({
        message: "no product"
      });
    } else {
      res.status(200).json(allProductData);
    }
  });
}



exports.getSingleProduct = function (req, res) {
  Product.aggregate([{
      $lookup: {
        from: "productvariants",
        localField: "_id",
        foreignField: "productId",
        as: "productdetails"
      },
    },
    {
      $match: {
        _id: ObjectId(req.params.id)
      }
    },
  ], function (err, allProductData) {
    if (err) {
      res.status(500).send({
        message: "no product"
      });
    } else {
      res.status(200).json(allProductData[0]);
    }
  });
} */

/* 
exports.deleteProduct = function (req, res) {
  Product.findByIdAndRemove(req.params.id, function (err, data) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      var s3 = new AWS.S3();
      s3.deleteObject({
        Bucket: env.Bucket,
        Key: 'images' + '/' + 'product' + '/' + req.params.id
      }, function (err, data) {
        if (err) {
          res.status(500).json(err);
        } else {
           Product.aggregate([{
             $lookup: {
               from: "productvariants",
               localField: "_id",
               foreignField: "productId",
               as: "productdetails"
             }
           }], function (err, allProductData) {
             if (err) {
               res.status(500).send({
                 message: "no product"
               });
             } else {
               res.status(200).json(allProductData);
             }
           });
          Product.find({}).select().exec(function (err, data) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(data);
            }
          });
        }
      });
    }
  });
} */

exports.deleteProduct = function (req, res) {
  Product.findByIdAndRemove(req.params.id, function (err, data) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      Product.find({}).select().exec(function (err, data) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          res.status(200).json(data);
        }
      });
    }
  });
}

exports.getProductPriceRange = function (req, res) {
  Product.find({
    sizePrice: {
      '$gte': req.body.price1,
      '$lte': req.body.price2
    }
  }).select().exec(function (err, findData) {
    if (err) {
      res.status(500).json(err);
    } else {
      const firstValue = findData.map(element => element.productId);
      console.log(firstValue);
      /*  Product.aggregate([{
           $lookup: {
             from: "productvariants",
             localField: '_id',
             foreignField: 'productId',
             as: "productdetails"
           }
         },
         {
           $match: {
             _id: {
               $in: firstValue
             }
           }

         }
       ], function (err, aggData) {
         if (err) {
           res.status(500).json(err);
         } else {
           res.status(200).json(aggData);
         }
       }) */
      Product.find({
        _id: {
          $in: firstValue
        }
      }, function (err, aggData) {
        if (err) {
          res.status(500).json(err);
        } else {
          res.status(200).json(aggData);
        }
      })
    }
  });
}
/* 
exports.updateProductImage = function (req, file, res) {
  var firstValue;
  firstValue = file.map(e => e.originalname);
  Product.findOneAndUpdate({
    "_id": req.params.id
  }, {
    $push: {
      productImageName: firstValue
    }
  }).select().exec(function (err, data) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  });
} */

exports.deleteSingleProductImage = function (req, res) {
  Product.find({
    '_id': req.params.id
  }, function (err, productDetail) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      Product.update({
        '_id': req.params.id
      }, {
        $pull: {
          productImageName: req.params.name
        }
      }).select().exec(function (err, data) {
        if (err) {
          res.status(500).json(err);
        } else {
          s3.deleteObject({
            Bucket: env.Bucket,
            Key: 'images' + '/' + 'product' + '/' + req.params.id + '/' + req.params.name
          }, function (err, data) {
            if (err) {
              res.status(500).json(err);
            } else {
              /*  Product.aggregate([{
                  $lookup: {
                    from: "productvariants",
                    localField: "_id",
                    foreignField: "productId",
                    as: "productdetails"
                  },
                },
                {
                  $match: {
                    _id: ObjectId(req.params.id)
                  }
                },
              ], function (err, allProductData) {
                if (err) {
                  res.status(500).send({
                    message: "no product"
                  });
                } else {
                  res.status(200).json(allProductData[0]);
                }
              });
            }
          }) */
              Product.findOne({
                _id: req.params.id
              }).select().exec(function (err, data) {
                if (err) {
                  res.status(500).send({
                    message: "Some error occurred while retrieving notes."
                  });
                } else {
                  res.status(200).json(data);
                }
              });
            }
          });
        }
      });
    }
  });
}

exports.sortProductByDate = function (req, res) {
  Product.find({}).sort({
    'dateAdded': req.body.dateSort
  }).exec(function (err, data) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  })
}
exports.sortProductByName = function (req, res) {
  Product.find({}).sort({
    'productName': req.body.nameSort
  }).exec(function (err, data) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  })
}

exports.productOverallCount = function (req, res) {
  Product.find({}).count().exec(function (err, data) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  })
}

// get single product With Brand
exports.getSingleProductWithBrand = function (req, res) {
  Product.aggregate([{
    $lookup: {
      from: "brands",
      localField: "brandId",
      foreignField: "_id",
      as: "brand"
    },
  },
  {
    $match: {
      _id: ObjectId(req.params.id)
    }
  },{
    $match: {
      'publish': true
    }
  }
  ], function (err, allProductData) {
    if (err) {
      res.status(500).send({
        message: "no product"
      });
    } else {
      res.status(200).json(allProductData[0]);
    }
  });
}
// exports.getAllProductMainCategory = function (req, res) {
//   Product.find({
//     mainCategoryId: req.params.maincatid
//   }).select().exec(function (err, data) {
//     if (err) {
//       res.status(500).send({
//         message: "Some error occurred while retrieving notes."
//       });
//     } else {
//       res.status(200).json(data);
//     }
//   });
// }
exports.getAllProductMainCategory =function(req,res){
  var pageNo = parseInt(req.body.pageNo);
  var perPage = parseInt(req.body.perPage);
  var skip =  pageNo > 0 ? ( ( pageNo - 1 ) * perPage ) : 0 ;
  Product.aggregate([
   
    { '$match'    : { mainCategoryId: ObjectId(req.params.maincatid),
      publish: true   } },

      {
        $project: {
          mainCategoryId: 1,
          productName: 1,
          price: 1,
          sp: 1,
          productImage: 1,
          discount: 1,
          'child.quantity': 1,
          seqOrder: 1,
          description: { $ifNull: ["$seqOrder", Infinity]}
        }
      },
      {
        $sort: {
         description: 1,
         seqOrder: -1
        }
      },
    { '$facet'    : {
        metadata: [ { $count: "total" }, { $addFields: { page: pageNo }}],
        data: [ { $skip: skip }, { $limit: perPage } ] 
    } }
  ] ).exec(function(err,data) {
    if (err) {
                            res.status(500).send({
                              message: "Some error occurred while retrieving notes."
                            });
                          } else {
                      
                            res.status(200).json(data);
                          }
  })
}
exports.pagination = function(req,res){

  var pageNo = parseInt(req.body.pageNo);
  var perPage = parseInt(req.body.perPage);
  var products = {}
  if(pageNo < 0 || pageNo === 0) {
        data = {"error" : true,"message" : "invalid page number, should start with 1"};
        return res.json(data)
  }
  products.skip = perPage * (pageNo - 1)
  products.limit = perPage
  
  Product.count({
    superCategoryId: req.body.supercatid,
    publish: true
  },function(err,totalCount) {
             if(err) {
               data = {"error" : true,"message" : "Error fetching data"}
             }
             Product.find({},{},products,function(err,data) {
            
            if(err) {
              res.status(500).json(error);
            } else {
                var totalPages = Math.ceil(totalCount / perPage)
                data = {"error" : false,"products" : data,"pages": totalPages};
            }
           res.status(200).json(data);
         });
       })

}
// function printStudents(pageNumber, nPerPage) {
//   print( "Page: " + pageNumber );
//   var pageNo = parseInt(req.body.pageNo);
//   var perPage = parseInt(req.body.perPage);
//   db.students.find({superCategoryId: req.params.supercatid,
//            publish: true})
//              .skip( pageNumber > 0 ? ( ( pageNumber - 1 ) * nPerPage ) : 0 )
//              .limit( nPerPage )
//              .forEach( student => {
//                print( student.name );
//              } );
// }
// db.Order.aggregate([
//   { '$match'    : { "company_id" : ObjectId("54c0...") } },
//   { '$sort'     : { 'order_number' : -1 } },
//   { '$facet'    : {
//       metadata: [ { $count: "total" }, { $addFields: { page: NumberInt(3) } } ],
//       data: [ { $skip: 20 }, { $limit: 10 } ] // add projection here wish you re-shape the docs
//   } }
// ] )
exports.getAllProductSuperCategory =function(req,res){
  var pageNo = parseInt(req.body.pageNo);
  var perPage = parseInt(req.body.perPage);
  var skip =  pageNo > 0 ? ( ( pageNo - 1 ) * perPage ) : 0 ;
  Product.aggregate([
   
    { '$match'    : { superCategoryId: ObjectId(req.params.supercatid),
      publish: true   } },
    /*   { '$group': {
        _id: {
          superCategoryId: '$superCategoryId',
          productName: '$productName',
           price: '$price',
            sp: '$sp',
             productImage: '$productImage',
              discount: '$discount',
              child: '$child.quantity',
              _id: '$_id'
        }
      }}, */
      {
        $project: {
          superCategoryId: 1,
          productName: 1,
          price: 1,
          sp: 1,
          productImage: 1,
          discount: 1,
          'child.quantity': 1,
          seqOrder: 1,
          description: { $ifNull: ["$seqOrder", Infinity]}
        }
      },
      {
        $sort: {
         description: 1,
         seqOrder: -1
        }
      },
    { '$facet'    : {
        metadata: [ { $count: "total" }, { $addFields: { page: pageNo }}],
        data: [ { $skip: skip }, { $limit: perPage } ] 
    } }
  ] ).exec(function(err,data) {
    if (err) {
                            res.status(500).send({
                              message: "Some error occurred while retrieving notes."
                            });
                          } else {
                      
                            res.status(200).json(data);
                          }
  })

  // Product.find({superCategoryId: req.params.supercatid,
  //          publish: true}).estimatedDocumentCount()
  //            .skip( pageNo > 0 ? ( ( pageNo - 1 ) * perPage ) : 0 )
  //            .limit( perPage ).exec(function (err, data){
  //             if (err) {
  //                       res.status(500).send({
  //                         message: "Some error occurred while retrieving notes."
  //                       });
  //                     } else {
  //                       // var totalPages = Math.ceil(totalData / perPage)
  //                       // data = {"error" : false,"product" : data,"pages": totalPages};
                       
  //                       res.status(200).json(data);
  //                     }
  //            })
             
}
// exports.getAllProductSuperCategory = function (req, res) {
  
 
//   if(req.body.pageNo === undefined){
//     var perPage = 30;
//     var pageNo = 1;
//    } else{
//      pageNo = parseInt(req.body.pageNo);
//      perPage = parseInt(req.body.perPage);
//   }
  
//   var paginatorDetails = {}
//   if(pageNo < 0 || pageNo === 0) {
//         data = {"error" : true,"message" : "invalid page number, should start with 1"};
//         return res.json(data)
//   }
//   paginatorDetails.superCategoryId = req.params.supercatid;
//   paginatorDetails.publish = true;
//   paginatorDetails.skip = perPage * (pageNo - 1);
//   paginatorDetails.limit = perPage;

//   Product.find({
    
//   }).select().exec(function (err, totalData) {
//     if(err) {
//       data = {"error" : true,"message" : "Error fetching data"}
//     } else {
//       Product.find({ },{},paginatorDetails,function(err,data) {
//       if (err) {
//         res.status(500).send({
//           message: "Some error occurred while retrieving notes."
//         });
//       } else {
//         var totalPages = Math.ceil(totalData.length / perPage)
//         data = {"error" : false,"product" : data,"pages": totalPages};
       
//         res.status(200).json(data);
//       }
//     });
//     }
      
// })
// }
// exports.getAllProductSuperCategory = function (req, res) {
//   var pageNo = parseInt(req.body.pageNo);
//   var perPage = parseInt(req.body.perPage);
//   var products = {}
//   if(pageNo < 0 || pageNo === 0) {
//         data = {"error" : true,"message" : "invalid page number, should start with 1"};
//         return res.json(data)
//   }
//   products.skip = perPage * (pageNo - 1);
//   products.limit = perPage;
//   Product.find({
//     superCategoryId: req.params.supercatid,
//     publish: true
//   }).select('productName price sp productImage discount').exec(function (err, totalData) {
//     if(err) {
//       data = {"error" : true,"message" : "Error fetching data"}
//     }
//     Product.find({  superCategoryId: req.params.supercatid,
//       publish: true },{},products,function(err,data) {
//     if (err) {
//       res.status(500).send({
//         message: "Some error occurred while retrieving notes."
//       });
//     } else {
//       var totalPages = Math.ceil(totalData.length / perPage)
//       data = {"error" : false,"products" : data,"pages": totalPages};
//       console.log(data);
//       res.status(200).json(data);
//     }
//   });
// })
// }
// exports.getAllProductSuperCategory = function (req, res) {
//   Product.find({
//     superCategoryId: req.params.supercatid,
//     publish: true
//   }).select('productName price sp productImage discount').exec(function (err, data) {
//     if (err) {
//       res.status(500).send({
//         message: "Some error occurred while retrieving notes."
//       });
//     } else {
//       console.log(data);
//       res.status(200).json(data);
//     }
//   });
// }
// exports.getAllProductSubCategory = function (req, res) {
//   Product.find({
//     subCategoryId: req.params.subcatid,
//     publish: true
//   }).select().exec(function (err, data) {
//     if (err) {
//       res.status(500).send({
//         message: "Some error occurred while retrieving notes."
//       });
//     } else {
//       res.status(200).json(data);
//     }
//   });
// }
exports.getAllProductSubCategory =function(req,res){
  var pageNo = parseInt(req.body.pageNo);
  var perPage = parseInt(req.body.perPage);
  var skip =  pageNo > 0 ? ( ( pageNo - 1 ) * perPage ) : 0 ;
  Product.aggregate([
   
    { '$match'    : { subCategoryId: ObjectId(req.params.subcatid),
      publish: true   } },
     /*  { '$group': {
        _id: {
          subCategoryId: '$subCategoryId',
          productName: '$productName',
           price: '$price',
            sp: '$sp',
             productImage: '$productImage',
              discount: '$discount',
              
              _id: '$_id'
        }
      }}, */
      {
        $project: {
          subCategoryId: 1,
          productName: 1,
          price: 1,
          sp: 1,
          productImage: 1,
          discount: 1,
          'child.quantity': 1,
          seqOrder: 1,
          description: { $ifNull: ["$seqOrder", Infinity]}
        }
      },
      {
        $sort: {
         description: 1,
         seqOrder: -1
        }
      },
    { '$facet'    : {
        metadata: [ { $count: "total" }, { $addFields: { page: pageNo }}],
        data: [ { $skip: skip }, { $limit: perPage } ] 
    } }
  ] ).exec(function(err,data){
    if (err) {
                            res.status(500).send({
                              message: "Some error occurred while retrieving notes."
                            });
                          } else {
                      
                            res.status(200).json(data);
                          }
  })

}

exports.updatePublishProduct = function (req, res) {
  var second = [];
  for (let i = 0; i <= req.body.length - 1; i++) {
    first = req.body[i]._id;
    second.push(first);
  }
  Product.updateMany({
    '_id': second
  }, {
    $set: {
      'publish': true
    }
  }).select().exec(function (err, data) {
    if (err) {
      res.status(500).json(err);
    } else {
      Product.find({}).select().exec(function (err, data) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          /* var dataLength = data.length - 1;
          for (var i = 0; i <= dataLength; i++) {
              data[i].bannerImageName = env.ImageServerPath + 'banners' + '/' + data[i]._id + '/' + data[i].bannerImageName;
          } */
          res.status(200).json(data);
        }
      });
    }
  })
}

exports.UpdateUnPublishProduct = function (req, res) {
  var second = [];
  for (let i = 0; i <= req.body.length - 1; i++) {
    first = req.body[i]._id;
    second.push(first);
  }
  Product.updateMany({
    '_id': second
  }, {
    $set: {
      'publish': false
    }
  }).select().exec(function (err, data) {
    if (err) {
      res.status(500).json(err);
    } else {
      Product.findOne({
        '_id': req.params.id
      }).select().exec(function (err, data1) {
        if (err) {
          res.status(500).json(err);
        } else {
          Product.find({}).select().exec(function (err, data) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              /*  var dataLength = data.length - 1;
               for (var i = 0; i <= dataLength; i++) {
                   data[i].bannerImageName = env.ImageServerPath + 'banners' + '/' + data[i]._id + '/' + data[i].bannerImageName;
               } */
              res.status(200).json(data);
            }
          });
        }
      })
    }
  })
}

exports.getProductForRow = function (req, res) {
  Product.find({'publish': true}).select().exec(function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(data);
    }
  });
}

// Demo
exports.getProductByIdDemo = function (req, res) {
  Product.find({ 'styleCode': req.params.id }).select().exec(function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(data);
    }
  });
}

// Product Child 
exports.getProductChild = function (req, res) {
  Product.findOne({
    '_id': req.params.id
  }).select().exec(function (err, findData) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(findData);
    }
  })
}

exports.getPromotionProduct = function (req, res) {
  Product.find({ '_id': req.body.promotionProudctId }).select().exec(function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(data);
    }
  });
}
// exports.searchProduct = function (req, res) {
//   Product.find({
//     $text: {
//       $search: req.body.search
//     }
//   }, {
//     score: {
//       $meta: "textScore"
//     }
//   }).sort({
//     score: {
//       $meta: "textScore"
//     }
//   }).exec(function (err, searchData) {
//     if (err) {
//       res.status(500).json(err);
//     } else {
//       res.status(200).json(searchData);
//     }
//   })
// }
exports.searchProduct =function(req,res){
  var pageNo = parseInt(req.body.pageNo);
  var perPage = parseInt(req.body.perPage);
  
  var skip =  pageNo > 0 ? ( ( pageNo - 1 ) * perPage ) : 0 ;
  Product.aggregate([
    { '$match'    : { $or: [
      { productName: { '$regex': new RegExp(req.body.searchId , 'i') } },
      { productDescription: { '$regex': new RegExp(req.body.searchId , 'i') } },
      { manufactureInfo: { '$regex': new RegExp(req.body.searchId , 'i') } },
      { superCategoryName: { '$regex': new RegExp(req.body.searchId , 'i') } },
      { mainCategoryName: { '$regex': new RegExp(req.body.searchId , 'i') } },
      { subCategoryName: { '$regex': new RegExp(req.body.searchId , 'i') } },
      { color: { '$regex': new RegExp(req.body.searchId , 'i') } },
      { catalogueName: { '$regex': new RegExp(req.body.searchId , 'i') } },
      { fabric: { '$regex': new RegExp(req.body.searchId , 'i') } },
      { searchTerms1: { '$regex': new RegExp(req.body.searchId , 'i') } },
      { searchTerms2: { '$regex': new RegExp(req.body.searchId , 'i') } },
      { searchTerms3: { '$regex': new RegExp(req.body.searchId , 'i') } },
      { searchTerms4: { '$regex': new RegExp(req.body.searchId , 'i') } },
      { searchTerms5: { '$regex': new RegExp(req.body.searchId , 'i') } },
      { pattern: { '$regex': new RegExp(req.body.searchId , 'i') } },
      { toFitWaist: { '$regex': new RegExp(req.body.searchId , 'i') } },
      { costIncludes: { '$regex': new RegExp(req.body.searchId , 'i') } },
      ]} },

      {
        $project: {
          superCategoryId:1,
          mainCategoryId:1,
          subCategoryId:1,
          productName: 1,
          price: 1,
          sp: 1,
          productImage: 1,
          discount: 1,
          'child.quantity': 1,
          seqOrder: 1,
          description: { $ifNull: ["$seqOrder", Infinity]}
        }
      },
     /*  { $project : { score: { $meta: "textScore" } } }, */
     /*  {
        $sort: {
         
          score: { $meta: "textScore" }
      
        }
      }, */
    { '$facet'    : {
        metadata: [ { $count: "total" }, { $addFields: { page: pageNo }}],
        data: [ { $skip: skip }, { $limit: perPage } ] 
    } }
  ] ).exec(function(err,data) {
    if (err) {
      res.status(500).send({
      message: "Some error occurred while retrieving notes."
    });
  } else {
  res.status(200).json(data);
    }
  })
}
exports.getAllProductBrand = function (req, res) {
  Product.find({
    brandId: req.params.brandid
  }).select().exec(function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(data);
    }
  });
}


exports.getAttributeProduct = function (req, res) {
  element = { "child.attribute": { $elemMatch: { attributeId: req.params.attributeId } } }
  Product.find({
    superCategoryId: req.params.supId,
  }, { element }).exec(function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(data);
    }
  });
}

/* exports.youMayAlsoLike = function (req, res) {
  var num = req.body.sp + 1000;
  Product.aggregate([
    {
      $match: {
        "catalogueName": req.body.catalogueName
      }
    },
    {
      $match: {
        $and: [
          {
            $or: [
              {
                "sp": { $gte: num, $lte: req.body.sp }
              },
              {
                "sp": { $gte: req.body.sp, $lte: num }
              }
            ]
          },
          {
            "fabric": req.body.fabric
          }
        ]
      }
    }
  ]).exec(function (err, aggData) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(aggData);
    }
  })
} */
exports.youMayAlsoLike = function (req, res) {
  var num = req.body.sp + 1000;
  Product.aggregate([
    {
      $match: {
        $and: [
          {
            "catalogueName": req.body.catalogueName
          },
          {
            $or: [
              {
                "fabric": req.body.fabric
              },
              {
                $or: [
                  {
                    "sp": { $gte: num, $lte: req.body.sp }
                  },
                  {
                    "sp": { $gte: req.body.sp, $lte: num }
                  }
                ]
              }
            ]
          }
        ]
      }
    }
  ]).exec(function (err, aggData) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(aggData);
    }
  })
}

exports.bulkWriteExample = function (req, res) {

  Product.bulkWrite(
    products.map((product) =>
      ({
        updateOne: {
          filter: { retailer: product._id },
          update: { $set: product },
          upsert: true
        }
      })
    ));
}

exports.multiDeleteProduct = function(req, res) {
  var second = [];
  for (let i = 0; i <= req.body.length - 1; i++) {
    first = req.body[i]._id;
    second.push(first);
  }
  Product.deleteMany({'_id': second}).select().exec(function(err, deleteData) {
    if (err) {
      res.status(500).json(err);
    } else {
      /* res.status(200).json(deleteData); */
      Product.find({}).select().exec(function(err, findData) {
        if (err) {
          res.status(500).json(err);
        } else {
          res.status(200).json(findData);
        }
      })
    }
  })
}


// exports.pagination = function(req,res){
//   var pageNo = req.body.page ; // parseInt(req.query.pageNo)
//   var size = req.body.perPage;
//   var query = {}
//   if (pageNo < 0 || pageNo === 0) {
//       response = { "error": true, "message": "invalid page number, should start with 1" };
//       return res.json(response)
//   }
//   query.skip = size * (pageNo - 1)
//   query.limit = size
  
//   Product.find ({},{},query,function(error,data){
//     if(error){
//       res.status(500).json(error);
//     }else{
//       res.status(200).json(data);
//     }
//   })
  
// }

// exports.pagination = function(req,res){

//   var pageNo = parseInt(req.body.pageNo);
//   var perPage = parseInt(req.body.perPage);
//   var products = {}
//   if(pageNo < 0 || pageNo === 0) {
//         data = {"error" : true,"message" : "invalid page number, should start with 1"};
//         return res.json(data)
//   }
//   products.skip = perPage * (pageNo - 1)
//   products.limit = perPage
  
//   Product.count({
//     superCategoryId: req.body.supercatid,
//     publish: true
//   },function(err,totalCount) {
//              if(err) {
//                data = {"error" : true,"message" : "Error fetching data"}
//              }
//              Product.find({},{},products,function(err,data) {
            
//             if(err) {
//               res.status(500).json(error);
//             } else {
//                 var totalPages = Math.ceil(totalCount / perPage)
//                 data = {"error" : false,"products" : data,"pages": totalPages};
//             }
//            res.status(200).json(data);
//          });
//        })

// }
// exports.getAllProductSuperCategory = function (req, res) {
//   Product.find({
//     superCategoryId: req.params.supercatid,
//     publish: true
//   }).select('productName price sp productImage discount').exec(function (err, data) {
//     if (err) {
//       res.status(500).send({
//         message: "Some error occurred while retrieving notes."
//       });
//     } else {
//       console.log(data);
//       res.status(200).json(data);
//     }
//   });
// }
exports.getSupercategoryAndFilter = function(req, res) {
  SuperCategory.findOne({'_id': req.params.supId}).select().exec(function(err, category) {
    if (err) {
      res.status(500).json(err);
    } else {
      FilterOption.find({}).select().exec(function (err, filterOption) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          if (category) {
            var superCatAttribute = category.attribute.filter(el => el.fieldType === 'Dropdown' && el.fieldEnable === true);
          } else {
            var superCatAttribute = category;
          }
          data = { "error": false, "category": category, "filterAttribute": superCatAttribute, "filterOption": filterOption};
          res.status(200).json(data);
        }
      })
    }
  })
}
exports.getSubcategoryAndFilter = function(req, res) {
  SuperCategory.aggregate([{ $unwind: "$mainCategory" },
  { $unwind: "$mainCategory.subCategory" }, {$match: {'mainCategory.subCategory._id': ObjectId(req.params.subId)}} ]).exec(function(err, category) {
    if (err) {
      res.status(500).json(err);
    } else {
      FilterOption.find({}).select().exec(function(err, filterOption) {
        if (err) {
          res.status(500).json(err);
        } else {
          if (category[0]) {
            var subCatAttribute = category[0].mainCategory.subCategory.attribute.filter(el => el.fieldType === 'Dropdown' && el.fieldEnable === true);
          } else {
            var subCatAttribute = category[0];
          }
          data = { "error": false, "category": category[0], "filterAttribute": subCatAttribute, "filterOption": filterOption};
          res.status(200).json(data);
        }
      })
    }
  } )
}
exports.getProductBySuperCategoryForSequence = function(req, res) {
  Product.find({'superCategoryId': req.params.supId}).select('productName productImage sku price seqOrder').exec(function(err ,product) {
    if (err) {
      res.status(500).json(err);
    } else {
      product = product.sort((a, b) => (a.seqOrder != null ? a.seqOrder : Infinity) - (b.seqOrder != null ? b.seqOrder : Infinity));
      res.status(200).json(product);
    }
  })
}
exports.getProductBySubCategoryForSequence = function(req, res) {
  Product.find({'subCategoryId': req.params.subId}).select('productName productImage sku price seqOrder').exec(function(err ,product) {
    if (err) {
      res.status(500).json(err);
    } else {
      product = product.sort((a, b) => (a.seqOrder != null ? a.seqOrder : Infinity) - (b.seqOrder != null ? b.seqOrder : Infinity));
      res.status(200).json(product);
    }
  })
}

exports.addSuperCategorySequenceOrder = function(req, res) {
  var sequence = [];
  for (let i = 0; i <= req.body.length - 1; i++) {
    order = req.body[i]._id;
    sequence.push(order);
  }
  Product.updateMany({
    'superCategoryId': req.params.id
  }, {$unset: {seqOrder:1}},{multi: true}).select().exec(function(err, updateProduct) {
    if (err) {
      res.status(500).json(err);
    } else {
      Product.find({'_id': sequence}).select().exec(function(err, product) {
        if (err) {
          res.status(500).json(err);
        } else {
          let num = 1;
          for (let i = 0; i <= sequence.length - 1; i++) {
            for (let j = 0; j <= product.length - 1; j++) {
              if (sequence[i] === product[j].id) {
                let a = 0
                a = num + i;
                product[j].seqOrder = a;
              }
          }
        }
          saveAll();
      function saveAll( callback ){
        var count = 0;
        product.forEach(function(doc){
            doc.save(function(err, saveData){
              if (err) {
                res.status(500).json(err);
              } else {
                count++;
                if( count == product.length ){
              /*    Product.find({'superCategoryId': req.params.id},{seqOrder: {'exists': true}}).sort({'seqOrder': 1}).select('productName productImage sku price seqOrder').exec(function(err, findData) {
                   if (err) {
                     res.status(500).json(err);
                   } else {
                    
                   findData = findData.sort((a, b) => (a.seqOrder != null ? a.seqOrder : Infinity) - (b.seqOrder != null ? b.seqOrder : Infinity));
                     res.status(200).json(findData);
                   }
                 }) */
                /*  Product.aggregate(
                  [
                     {
                        $project: {
                           productName: 1,
                           description: { $ifNull: [ "$seqOrder", -1*(<mimimum value>)* ] }
                        }
                     },
                     { 
                        $sort : { 
                          seqOrder : 1
                        }
                     }
                  ]
               ) */
               Product.aggregate([
                 
                 {
                   $project: {
                     productName: 1,
                     seqOrder: 1,
                     productImage: 1,
                     sku: 1,
                     price: 1,
                     superCategoryId: 1,
                     description: { $ifNull: ["$seqOrder", Infinity]}
                   }
                 },
                 {
                  $match: {
                    'superCategoryId': ObjectId(req.params.id)
                  }
                },
                 {
                   $sort: {
                    description: 1,
                    seqOrder: -1
                   }
                 }
               ]).exec(function(err, data) {
                 if (err) {
                   res.status(500).json(err);
                 } else {
                   res.status(200).json(data);
                 }
               })
                }
              }
            });
        });
      }
        }
      })
    }
  })
}

exports.addSubCategorySequenceOrder = function(req, res) {
  var sequence = [];
  for (let i = 0; i <= req.body.length - 1; i++) {
    order = req.body[i]._id;
    sequence.push(order);
  }
  Product.updateMany({
    'subCategoryId': req.params.id
  }, {$unset: {seqOrder:1}},{multi: true}).select().exec(function(err, updateProduct) {
    if (err) {
      res.status(500).json(err);
    } else {
      Product.find({'_id': sequence}).select().exec(function(err, product) {
        if (err) {
          res.status(500).json(err);
        } else {
          let num = 1;
          for (let i = 0; i <= sequence.length - 1; i++) {
            for (let j = 0; j <= product.length - 1; j++) {
              if (sequence[i] === product[j].id) {
                let a = 0
                a = num + i;
                product[j].seqOrder = a;
              }
          }
        }
          saveAll();
      function saveAll( callback ){
        var count = 0;
        product.forEach(function(doc){
            doc.save(function(err, saveData){
              if (err) {
                res.status(500).json(err);
              } else {
                count++;
                if( count == product.length ){
              /*    Product.find({'subCategoryId': req.params.id},{seqOrder: {'exists': true}}).sort({'seqOrder': 1}).select('productName productImage sku price seqOrder').exec(function(err, findData) {
                   if (err) {
                     res.status(500).json(err);
                   } else {
                    
                   findData = findData.sort((a, b) => (a.seqOrder != null ? a.seqOrder : Infinity) - (b.seqOrder != null ? b.seqOrder : Infinity));
                     res.status(200).json(findData);
                   }
                 }) */
                 Product.aggregate([
                 
                  {
                    $project: {
                      productName: 1,
                      seqOrder: 1,
                      productImage: 1,
                      sku: 1,
                      price: 1,
                      subCategoryId: 1,
                      description: { $ifNull: ["$seqOrder", Infinity]}
                    }
                  },
                  {
                   $match: {
                     'subCategoryId': ObjectId(req.params.id)
                   }
                 },
                  {
                    $sort: {
                     description: 1,
                     seqOrder: -1
                    }
                  }
                ]).exec(function(err, data) {
                  if (err) {
                    res.status(500).json(err);
                  } else {
                    res.status(200).json(data);
                  }
                })
                }
              }
            });
        });
      }
        }
      })
    }
  })
}

exports.getInventoryQty = function(req, res) {
  Product.aggregate([
    {
      '$unwind': '$child'
    },
    {
      $group: {
        _id: null,
        totalAmount: {
          $sum: '$child.quantity'
        }
      }
    }
  ]).exec(function(err, aggData) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(aggData);
    }
  })
}
exports.getCategoryWiseProductCount = function(req, res) {
  Product.aggregate([
  {
    $group: {
      _id: {
        category: '$superCategoryName',
      },
      count:{$sum:1}
    }
  }
  ]).exec(function(err, aggData) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(aggData);
    }
  })
}
exports.getOutOfStockQtyCount = function(req, res) {
  Product.aggregate([
    {
      '$unwind': '$child'
    },
  {
    $match: {
      'child.quantity': 0
    }
  },
  {
    $count: "outOfStock"
  }
  ]).exec(function(err, aggData) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(aggData);
    }
  })
}
exports.getProductBySuperCategory = function(req, res) {
  Product.find({superCategoryId: req.params.id, vendorId: req.params.vendorID}).select().exec(function(err, category) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(category);
    }
  })
}
exports.getProductBySubCategory = function(req, res) {
  Product.find({subCategoryId: req.params.id, vendorId: req.params.vendorID}).select().exec(function(err, category) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(category);
    }
  })
}