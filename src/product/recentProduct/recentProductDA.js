var RecentProduct = require('../../model/recentProduct.model');
var Product = require('../../model/product.model');

exports.addrecentProduct = function(req, res) {
    RecentProduct.findOne({'userId': req.body.userId}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if(findData === null || findData === undefined) {
                var create = new RecentProduct();
                create.userId = req.body.userId;
                create.productId.push(req.body.productId);
                create.save(function(err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            } else {
            if (findData.productId.length <= 25) {
                findData.productId.push(req.body.productId);
                findData.save(function(err, directSavingData) {
                    if(err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(directSavingData);
                    }
                })
            } else {
                findData.productId.shift();
                findData.productId.push(req.body.productId);
                findData.save(function(err, changeSavingData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(changeSavingData);
                    }
                })
            }
        }

        }
    })
}
exports.getRecentProductByUser = function(req, res) {
    RecentProduct.findOne({'userId': req.params.id}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if(findData) {
                const product = findData.productId.reverse()
                Product.find({'_id': product}).limit(25).exec(function(err, productData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(productData);
                    }
                })
            } else {
                res.status(200).json(findData);
            }
            
           /*  Product.aggregate([{
                $lookup: {
                    from: findData.toString(),
                    localField: "_id",
                    foreignField: JSON.stringify(findData.productId),
                    as: "productDetails" 
                }
            }], function(err, aggregateData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(aggregateData);
                }
            }) */
        }
    })
}
exports.getRecentProudct = function(req, res) {
    RecentProduct.find({'userId': req.params.id}).select().exec(function(err, findData){
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}
exports.getProductForLocalStorage = function(req, res) {
    Product.find({'_id': req.body.productId}).select().exec(function(err, productData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(productData);
        }
    })
}