var RecentProductDA = require('./recentProductDA');

exports.addrecentProduct = function(req, res) {
    try {
        RecentProductDA.addrecentProduct(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getRecentProductByUser = function(req, res) {
    try {
        RecentProductDA.getRecentProductByUser(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getRecentProudct = function(req, res) {
    try {
        RecentProductDA.getRecentProudct(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getProductForLocalStorage = function(req, res) {
    try {
        RecentProductDA.getProductForLocalStorage(req, res);
    } catch (error) {
        console.log(error);
    }
}