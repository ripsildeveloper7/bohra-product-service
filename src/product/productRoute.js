'use strict';

var productMgr = require('./product/productMgr');
var productEditMgr = require('./product-edit/productEditMgr');
var excelMgr = require('./uploadExcel/excelMgr');
var upload = require('../config/multer.config');
var recentProductMgr = require('./recentProduct/recentProductMgr');
var InventoryMgr = require('./inventory-management/inventoryMgr');

module.exports = function (app) {
  app.route('/addproduct')
    .post(productMgr.addProduct); // add product details
  app.route('/getchildproduct')
    .get(productMgr.getChildProduct); // add product details
  app.route('/uploadproduct')
    .post(productMgr.uploadProduct); // upload product details

    app.route('/updatebulkupload/:id')
    .put(productMgr.updateBulkUpload);
    app.route('/parentproductimageupdate/:id')
    .put(productMgr.parentProudctImageUpdate);
    app.route('/childproductimageupdate/:id/:childId')
    .put(productMgr.childProudctImageUpdate);
  /*  app.route('/productimage/:id')
     .put(upload.array("uploads[]"), productMgr.addProductImage); */ // Upload Product Image
  app.route('/editproductcategory/:id')
    .put(productMgr.editProductCategory);
  app.route('/editproductbrand/:id')
    .put(productMgr.editProductBrand);
  app.route('/addsizevariant/:id')
    .put(productMgr.addSizeVariant);
  app.route('/editproductvariant/:id/productvariant/:productVariantId')
    .put(productMgr.editProductVariant);
  app.route('/product') // all product view
    .get(productMgr.getProduct);
    app.route('/allproductforadmin') // all product view
    .put(productMgr.getAllProductForAdmin);

  /* app.route('/productforui') // all product view for UI
    .get(productMgr.getProductForUI); */

  app.route('/productsingle/:id') // single product view
    .get(productMgr.getSingleProduct);

  app.route('/deleteproduct/:id') // delete product
    .delete(productMgr.deleteProduct);
  app.route('/editproductinfo/:id') // edit productinfo edit
    .put(productMgr.editProductInfo);
  app.route('/editproductseo/:id')
    .put(productMgr.editProductSeo); // edit seo edit
  

  app.route('/getproductpricerange')
    .post(productMgr.getProductPriceRange);
  /* app.route('/editproductimage/:id') // edit product image
    .put(upload.array("uploads[]"), productMgr.updateProductImage); */
  app.route('/deletesingleimage/:id/imagename/:name') // delete single image
    .delete(productMgr.deleteSingleProductImage); // delete product Variant
  app.route('/deleteproductvariant/:id/productvariant/:productVariantId')
    .delete(productMgr.deleteProductVariant);
  /*  app.route('/deletesingleimage/:id')
     .post(productMgr.deleteSingleProductImage); */
  app.route('/sortproductbydate')
    .post(productMgr.sortProductByDate); // sort product By Date  

  app.route('/sortproductbyname')
    .post(productMgr.sortProductByName); // sort product By Name

  app.route('/productoverallcount')
    .get(productMgr.productOverallCount);
  app.route('/singleproductwithbrand/:id')
    .get(productMgr.getSingleProductWithBrand);
  app.route('/allmaincategory/:maincatid')
    .put(productMgr.getAllProductMainCategory);
  app.route('/allsupercategory/:supercatid')
    .put(productMgr.getAllProductSuperCategory);
  app.route('/allsubcategory/:subcatid')
    .put(productMgr.getAllProductSubCategory);
  app.route('/getproductforrow')
    .get(productMgr.getProductForRow);

  app.route('/getproductbyiddemo/:id')
    .get(productMgr.getProductByIdDemo);  

  app.route('/brandimage/:id')
  app.route('/excel/:attributeId')
  .put(upload.single('excel'), excelMgr.uploadSingleExcel);
  /*     app.route('/relatedproducts/:stylecode/product/:id')
        .get(productMgr.relatedProducts);

    app.route('/product/:productId')
        .get(productMgr.getProductById);

    app.route('/productimages/:skuCode')
        .put(productMgr.createProductImage);

        app.route('/product/:id/region/:regionid')
        .put(productMgr.editRegionDetails);

        app.route('/mfdqty/:id')
        .put(productMgr.editQtyDetails);
 */

  app.route('/productimage/:id/childimage/:childid')
    .put(productEditMgr.uploadMultiImage);
  app.route('/editproductimage/:id') // edit product image
    .put(productEditMgr.uploadMultiImageEdit);
  /* app.route('/editproductdiscount/:id')
    .put(productMgr.editProductDiscount); */
    app.route('/editchildattribute/:id/:childId')
    .put(productEditMgr.editChildAttribute);
    app.route('/editproductparentinfo/:id')
    .put(productEditMgr.editParentProductInfo);
  app.route('/editproductchildinfo/:id/:childId')
    .put(productEditMgr.editChildProductInfo);
    
  app.route('/getproductchild/:id')
  .get(productMgr.getProductChild);


  app.route('/updatepublishproduct')
  .put(productMgr.updatePublishProduct);
  app.route('/updateunpublishproduct')
  .put(productMgr.UpdateUnPublishProduct);
  /* app.route('/editproductdiscount/:id')
    .put(productEditMgr.editProductDiscount); // edit discount edit
    app.route('/editproductchildinfo/:id/:childId')
    .put(productEditMgr.editChildProductInfo); */
    app.route('/getpromotionproduct')
    .post(productMgr.getPromotionProduct);  
    app.route('/searchproduct')
    .post(productMgr.searchProduct);
// Vendor

app.route('/updatequentitybyvendor/:id/:childId')
    .post(productEditMgr.updateQuantityByVendor);

// Recent Product
app.route('/addrecentproduct')
    .post(recentProductMgr.addrecentProduct);
app.route('/getrecentproductbyuser/:id')
    .get(recentProductMgr.getRecentProductByUser);    
app.route('/getrecentproduct/:id')
    .get(recentProductMgr.getRecentProudct);
app.route('/getproductforlocalrecent')
    .post(recentProductMgr.getProductForLocalStorage);            

app.route('/getcategory/:supId/attribute/:attributeId/attributefield/:attributefieldId')
    .get(productMgr.getAttributeProduct);  
    app.route('/allproductbybrand/:brandid')
    .get(productMgr.getAllProductBrand);  

    // you may also like

    app.route('/getyoumayalsolike')
    .post(productMgr.youMayAlsoLike);  

    // delete multi product
    app.route('/deletemultiproduct')
    .post(productMgr.multiDeleteProduct);


    app.route('/pagination')
    .put(productMgr.pagination);
   

   //excel
    app.route('/supercategoryexceldropdown/:supId')
    .get(excelMgr.superCategoryExceldropdown);
    app.route('/supercategoryexceldropdown/:supId/main/:mainId')
    .get(excelMgr.mainCategoryExceldropdown);
    app.route('/supercategoryexceldropdown/:supId/main/:mainId/sub/:subId')
    .get(excelMgr.subCategoryExceldropdown);

    app.route('/getsupercategoryandfilter/:supId')
    .get(productMgr.getSupercategoryAndFilter);
    app.route('/getsubcategoryandfilter/:subId')
    .get(productMgr.getSubcategoryAndFilter);

    app.route('/getproductbysupercategoryforsequence/:supId')
    .get(productMgr.getProductBySuperCategoryForSequence);
    app.route('/getprdouctbysubcategoryforsequence/:subId')
    .get(productMgr.getProductBySubCategoryForSequence);
    app.route('/addsequenceforsupercategory/:id')
    .put(productMgr.addSuperCategorySequenceOrder);
    app.route('/addsequenceforsubcategory/:id')
    .put(productMgr.addSubCategorySequenceOrder);

    // stock
 

    app.route('/updateinventorybyshipping')
    .post(InventoryMgr.updateInventoryByShipping);
    app.route('/getinventoryquantity')
    .get(productMgr.getInventoryQty);
    app.route('/getcategorywiseproductcount')
    .get(productMgr.getCategoryWiseProductCount);
    app.route('/getoutofstockqtycount')
    .get(productMgr.getOutOfStockQtyCount);
    app.route('/getproductbysupercategory/:id/:vendorID')
    .get(productMgr.getProductBySuperCategory);
    app.route('/getprdouctbysubcategory/:id/:vendorID')
    .get(productMgr.getProductBySubCategory);
 
}