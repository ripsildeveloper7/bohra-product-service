var productEditDA = require('./productEditDA');
var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');
const AWS = require('aws-sdk');
/* 
exports.uploadSingleImage = function (req, res) {
  try {
    imagesDA.uploadSingleImage(req, res);
  } catch (error) {
    console.log(error);
  }
}
 */

exports.uploadMultiImage = function (req, res) {
  try {
    productEditDA.uploadMultiImage(req,  res);
  } catch (error) {
    console.log(error);
  }
} 


exports.uploadMultiImageEdit = function (req, res) {
  
  try {
    productEditDA.uploadMultiImageEdit(req, res);
  } catch (error) {
    console.log(error);
  }
  }
  exports.editChildAttribute = function (req, res) {
  
    try {
      productEditDA.editChildAttribute(req, res);
    } catch (error) {
      console.log(error);
    }
    }
  exports.editParentProductInfo = function (req, res) {
  
    try {
      productEditDA.editParentProductInfo(req, res);
    } catch (error) {
      console.log(error);
    }
    }

exports.editChildProductInfo = function (req, res) {
  
  try {
    productEditDA.editChildProductInfo(req, res);
  } catch (error) {
    console.log(error);
  }
  }

/* exports.uploadSingleImageMulter = function (req, res) {
  try {
    const params = {
      Bucket: env.Bucket + '/' + 'images' + '/' + 'product' + '/' + req.params.id, // create a folder and save the image
      Key: req.file.originalname,
      ACL: 'public-read',
      Body: req.file.buffer,
    };

    s3.upload(params, function (err, data) {
      if (err) {
        console.log(err);
      } else {
        productImageDA.uploadSingleImageMulter(req, data, res);
      }
    });
  } catch (error) {
    console.log(error);
  }
} */

exports.updateQuantityByVendor = function (req, res) {
  
  try {
    productEditDA.updateQuantityByVendor(req, res);
  } catch (error) {
    console.log(error);
  }
}