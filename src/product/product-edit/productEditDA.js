var Product = require('./../../model/product.model');


exports.uploadMultiImage = function (req, res) {
    Product.findOne({
        '_id': req.params.id
      }, function (err, data) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          data.productImage = req.body;
          data.dateModified = Date.now();
          data.save(function (err, productImageSave) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              /* res.status(200).json(productImageSave); */
              
              Product.findOne({
                '_id': req.params.id
              }, function(err, findData) {
                if(err) {
                  res.status(500).json(err);
                } else {
              var childValue = findData.child.id(req.params.childid);
              childValue.productImage = req.body;
               findData.save(function(err, saveData) {
                 if(err) {
                   res.status(500).json(err);
                 } else {
                   res.status(200).json(saveData);
                 }
               })
                }
              })
            }
          });
        }
      });
}


exports.uploadMultiImageEdit = function (req, res) {
    var firstValue;
    firstValue = req.body.productImageName;;
    Product.findOneAndUpdate({
      "_id": req.params.id
    }, {
      $push: {
        productImageName: firstValue
      }
    }).select().exec(function (err, data) {
      if (err) {
        res.status(500).json(err);
      } else {
        Product.findOne({
          _id: req.params.id
        }).select().exec(function (err, editProductData) {
          if (err) {
            res.status(500).send({
              message: "Some error occurred while retrieving notes."
            });
          } else {
            res.status(200).json(editProductData);
          }
        });
      }
    });
  }

  


exports.editProductDiscount = function (req, res) {
  
  Product.findOneAndUpdate({
    "_id": req.params.id
  }, {
  discount: req.body.discount
  }).select().exec(function (err, data) {
    if (err) {
      res.status(500).json(err);
    } else {
      Product.findOne({
        _id: req.params.id
      }).select().exec(function (err, editProductData) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
        } else {
          res.status(200).json(editProductData);
        }
      });
    }
  });
}
exports.editParentProductInfo = function(req, res) {
  Product.findOne({'_id': req.params.id}).select().exec(function(err, product) {
    if (err) {
      res.status(500).json(err);
    } else {
      product.productName = req.body.productName,
      product.manufactureInfo = req.body.manufactureInfo,
      product.hsnCode  = req.body.hsnCode,
     /*  product.gstIn   = req.body.gstIn,// */
      product.sku   = req.body.sku,
      product.styleCode  = req.body.styleCode,
      product.variation   = req.body.variation,
      product.variationType   = req.body.variationType,
      product.sizeVariant  = req.body.sizeVariant,
      product.PINTsku   = req.body.PINTsku,
      product.weight  = req.body.weight,
      product.fabric   = req.body.fabric,
      /* productChildEdit.washCare   = req.body.washCare,//
      productChildEdit.productDimension   = req.body.productDimension,//
      productChildEdit.packingDimension   = req.body.packingDimension,// */
      product.discount   = req.body.discount,
      product.ttsPortol   = req.body.ttsPortol,
      product.ttsVendor   = req.body.ttsVendor,
      product.vp   = req.body.vp,
      product.sp   = req.body.sp,
      product.price   = req.body.sp,
      product.mrp   = req.body.mrp,
      product.quantity   = req.body.quantity,
      product.color   = req.body.color,
      product.pattern   = req.body.pattern,
      product.productDescription   = req.body.productDescription,
      product.toFitWaist   = req.body.toFitWaist,
      product.searchTerms1   = req.body.searchTerms1,
      product.searchTerms2   = req.body.searchTerms2,
      product.searchTerms3   = req.body.searchTerms3,
      product.searchTerms4   = req.body.searchTerms4,
      product.searchTerms5   = req.body.searchTerms5,
      product.colorId = req.body.colorId,
      product.save(function(err, updateData) {
        if (err) {
          res.status(500).json(err);
        } else {
          Product.findOne({
            _id: req.params.id
          }).select().exec(function (err, data) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(data);
            }
          });
        }
      })
    }
  })
}
exports.editChildAttribute = function(req, res) {
  Product.findOne({'_id': req.params.id}).select().exec(function(err, findData) {
    if (err) {
      res.status(500).json(err);
    } else {
      const child = findData.child.id(req.params.childId);
      child.attribute = req.body.attribute;
      findData.save(function(err, updateData) {
        if (err) {
          res.status(500).json(err);
        } else {
          Product.findOne({
            _id: req.params.id
          }).select().exec(function (err, data) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(data);
            }
          });
        }
      })
    }
  })
}
exports.editChildProductInfo = function(req, res) {
  Product.findOne({
    _id: req.params.id
  }).select().exec(function (err, product) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      var productChildEdit = product.child.id(req.params.childId);
      productChildEdit.productName = req.body.productName,
      productChildEdit.manufactureInfo = req.body.manufactureInfo,
      productChildEdit.hsnCode = req.body.hsnCode,
      /* productChildEdit.gstIn = req.body.gstIn, */
      productChildEdit.sku = req.body.sku,
      productChildEdit.styleCode = req.body.styleCode,
      productChildEdit.variation = req.body.variation,
      productChildEdit.variationType = req.body.variationType,
      productChildEdit.sizeVariant = req.body.sizeVariant,
      productChildEdit.PINTsku = req.body.PINTsku,
      productChildEdit.weight = req.body.weight,
      productChildEdit.fabric = req.body.fabric,
      /* productChildEdit.washCare   = req.body.washCare,//
      productChildEdit.productDimension   = req.body.productDimension,//
      productChildEdit.packingDimension   = req.body.packingDimension,// */
      productChildEdit.discount = req.body.discount,
      productChildEdit.ttsPortol = req.body.ttsPortol,
      productChildEdit.ttsVendor = req.body.ttsVendor,
      productChildEdit.vp = req.body.vp,
      productChildEdit.sp = req.body.sp,
      productChildEdit.price = req.body.sp,
      productChildEdit.mrp = req.body.mrp,
      productChildEdit.quantity = req.body.quantity,
      productChildEdit.color  = req.body.color,
      productChildEdit.pattern  = req.body.pattern,
      productChildEdit.productDescription  = req.body.productDescription,
      productChildEdit.toFitWaist  = req.body.toFitWaist,
      productChildEdit.searchTerms1  = req.body.searchTerms1,
      productChildEdit.searchTerms2  = req.body.searchTerms2,
      productChildEdit.searchTerms3  = req.body.searchTerms3,
      productChildEdit.searchTerms4  = req.body.searchTerms4,
      productChildEdit.searchTerms5  = req.body.searchTerms5
      productChildEdit.colorId = req.body.colorId,
      product.save(function (err, product) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
          console.log(err);
        } else {
          /*  Product.aggregate([{
               $lookup: {
                 from: "productvariants",
                 localField: "_id",
                 foreignField: "productId",
                 as: "productdetails"
               },
             },
             {
               $match: {
                 _id: ObjectId(product.productId)
               }
             },
           ], function (err, allProductData) {
             if (err) {
               res.status(500).send({
                 message: "no product"
               });
             } else {
               res.status(200).json(allProductData[0]);
             }
           }); */
          Product.findOne({
            _id: req.params.id
          }).select().exec(function (err, data) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(data);
            }
          });
        }
      });
    }
  });
}
exports.updateQuantityByVendor = function (req, res) {
  Product.findOne({
    _id: req.params.id
  }).select().exec(function (err, product) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      var productChildEdit = product.child.id(req.params.childId);
      productChildEdit.quantity = req.body.quantity,
      product.save(function (err, product) {
        if (err) {
          res.status(500).send({
            message: "Some error occurred while retrieving notes."
          });
          console.log(err);
        } else {
          Product.find({}).select().exec(function (err, data) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(data);
            }
          });
        }
      });
    }
  });
}