var InventoryDA = require('./inventoryDA');

exports.updateInventoryByShipping = function(req, res) {
    try {
        InventoryDA.updateInventoryByShipping(req, res);
    } catch (error) {
        console.log(error);
    }
}