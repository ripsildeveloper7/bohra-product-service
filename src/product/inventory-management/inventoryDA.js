var Product = require('../../model/product.model');

exports.updateInventoryByShipping = function(req, res) {
    var items = req.body.cart;
    Promise.all(
        items.map(function(doc) {
            return Product.findOneAndUpdate({
              _id: doc.productId, "child.INTsku": doc.INTsku
            }, { $inc: { "child.$.quantity": -doc.qty } });
        })
    ).then(function(documents) {
        res.status(200).json(documents);
    });
}