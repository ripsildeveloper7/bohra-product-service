
'use strict';
var brandMgr = require('./brandMgr');
var upload = require('../config/multer.config');

module.exports = function (app) {

    app.route('/addbrand')
        .post(brandMgr.addBrandDetails); // Add Brand Image

    app.route('/getbrand')   // get Brand 
        .get(brandMgr.getBrand);
    app.route('/deletebrand/:id')
        .delete(brandMgr.deleteBrand);  // Delete Brand
    app.route('/getsinglebrand/:id')
        .get(brandMgr.findSingleBrand);  // single  Brand
    app.route('/updatebrand/:id')
        .put(brandMgr.updateBrand);  // Update Banner Detail        
    app.route('/brandimage/:id')
        .put(brandMgr.uploadBrandImagesName);  
}
