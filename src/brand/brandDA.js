'use strict';
var Brand = require('./../model/brand.model');
var s3 = require('../config/s3.config');
var env = require('../config/s3.env');
const AWS = require('aws-sdk');

exports.addBrand = function (req, res) {
  var brand = new Brand();
  brand.brandName = req.body.brandName;
  brand.brandTitle = req.body.brandTitle;
  brand.brandDescription = req.body.brandDescription;
  brand.brandContent = req.body.brandContent;
  brand.brandStatus = req.body.brandStatus;
  brand.metaTitle = req.body.metaTitle;
  brand.metaDescription = req.body.metaDescription;
  brand.metaContent = req.body.metaContent;
  brand.save(function (err, brandData) {
    if (err) { // if it contains error return 0
      res.status(500).send({
        "result": 0
      });
    } else {
      res.status(200).json(brandData)
    }
  });
}


exports.getBrand = function (req, res) {
  Brand.find({}).select().exec(function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      res.status(200).json(data);
    }
});
}

exports.deleteBrand = function (req, res) {
  Brand.findOne({
    '_id': req.params.id
  }, function (err, brandDetails) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      s3.deleteObject({
        Bucket: env.Bucket,
        Key: 'images' + '/' + 'brand' + '/' + req.params.id + '/' + brandDetails.brandImageName
      }, function (err, bucketImage) {
        if (err) {
          res.status(500).json(err);
        } else {
          Brand.findByIdAndRemove(req.params.id, function (err, data) {
            if (err) {
              res.status(500).send({
                "result": 0
              });
            } else {
              Brand.find({}).select().exec(function (err, allbrand) {
                if (err) {
                  res.status(500).send({
                    message: "Some error occurred while retrieving notes."
                  });
                } else {
                  res.status(200).json(allbrand);
                }
              });
            }
          });
        }
      });
    }
  });
}

exports.findSingleBrand = function (req, res) {
  Brand.findOne({
    '_id': req.params.id
  }).select().exec(function (err, data) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(data);
    }
  });
}


exports.updateBrand = function (req, res) {
  Brand.findOne({
    '_id': req.params.id
  }).select().exec(function (err, brand) {
    if (err) {
      res.status(500).json(err);
    } else {
      brand.brandName = req.body.brandName;
      brand.brandTitle = req.body.brandTitle;
      brand.brandDescription = req.body.brandDescription;
      brand.brandContent = req.body.brandContent;
      brand.brandStatus = req.body.brandStatus;
      brand.metaTitle = req.body.metaTitle;
      brand.metaDescription = req.body.metaDescription;
      brand.metaContent = req.body.metaContent;
      brand.save(function (err, data) {
        if (err) {
          res.status(500).json(err);
        } else {
          res.status(200).json(data);
        }
      });
    }
  });
}


exports.uploadBrandImagesName = function (req,  res) {
  Brand.findOne({
    '_id': req.params.id
  }, function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      data.brandImageName = req.body.brandImageName;
      data.save(function (err, brandImageSave) {
        if (err) {
          res.status(500).send({
            message: 1
          });
        } else {
          Brand.find({}).select().exec(function (err, data) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(data);
            }
          });
        }
      });
    }
  });
}