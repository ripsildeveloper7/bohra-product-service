var SuperCategory = require('../../model/superCategory.model');


exports.SupCategoryAttributeFieldValue = function (req, res) {

  SuperCategory.findOne({ _id: req.params.supId }, function (err, category) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      var supCatAtt = category.attribute.id(req.params.attributeId);
      var supCatfieldValue = supCatAtt.fieldValue.id(req.params.fieldId);
      supCatfieldValue.fieldAttributeValue = req.body.fieldAttributeValue;
      category.save(function (err, result) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
          SuperCategory.findOne({ _id: req.params.supId }, function (err, finddata) {
            if (err) {
              res.status(500).json(err);
            } else {
              res.status(200).json(finddata);
            }
          })
        }
      });
    }
  });
}

exports.SupCategoryAttributeFieldValueDelete = function (req, res) {
  SuperCategory.findOne({ _id: req.params.supId }, function (err, category) {
    if (err) {
      res.status(500).send({
        "result": 0  
      });
    } else {
      var supCatAtt = category.attribute.id(req.params.attributeId);
      supCatAtt.fieldValue.remove(req.params.fieldId);
      category.save(function (err, result) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
          SuperCategory.findOne({ _id: req.params.supId }).select().exec(function (err, finddata) {
            if (err) {
              res.status(500).json(err);
            } else {
              res.status(200).json(finddata);
            }
          })
        }
      });
    }
  });
}

exports.SupCategoryAttribute = function (req, res) {

  SuperCategory.findOne({ _id: req.params.supId }, function (err, category) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      var supCatAtt = category.attribute.id(req.params.attributeId);
      supCatAtt.fieldName = req.body.fieldName;
      supCatAtt.fieldType = req.body.fieldType;
      supCatAtt.fieldSetting = req.body.fieldSetting;
      supCatAtt.fieldEnable = req.body.fieldEnable;
      supCatAtt.fieldEnableValue = req.body.fieldEnableValue;
      supCatAtt.sortOrder = req.body.sortOrder;
      category.save(function (err, result) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
console.log(result);
          res.status(200).json(result);
        }
      });
    }
  });
}

exports.SupCategoryAttributeDelete = function (req, res) {

  SuperCategory.findOne({ _id: req.params.supId }, function (err, category) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      category.attribute.remove(req.params.attributeId);
      category.save(function (err, result) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
          res.status(200).json(result);
        }
      });
    }
  });
}

exports.SupCategoryAttributeDelete = function (req, res) {

  SuperCategory.findOne({ _id: req.params.supId }, function (err, category) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      category.attribute.remove(req.params.attributeId);
      category.save(function (err, result) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
          res.status(200).json(result);
        }
      });
    }
  });
}


exports.SupCategoryAttributeAdd = function (req, res) {
  var attributeData = {
    attribute: req.body.attribute
  };
  SuperCategory.findOneAndUpdate({
    _id: req.params.supId
  }, {
    $push: {
      attribute: attributeData.attribute
    }
  }, function (err, finddata) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      SuperCategory.findOne({ _id: req.params.supId }).select().exec(function (err, finddata) {
        if (err) {
          res.status(500).json(err);
        } else {
          res.status(200).json(finddata);
        }
      })
    }
  });
}

exports.SupCategoryAttributeFieldValueAdd = function (req, res) {
  var attributeData = {
    fieldValue: req.body.fieldValue
  };
  SuperCategory.findOneAndUpdate({
    _id: req.params.supId, 'attribute._id': req.params.attributeId
  }, {
    $push: {
      'attribute.$.fieldValue': attributeData.fieldValue
    }
  }, function (err, find) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      SuperCategory.findOne({ _id: req.params.supId }).select().exec(function (err, finddata) {
        if (err) {
          res.status(500).json(err);
        } else {

          res.status(200).json(finddata);
        }
      })
    }
  });
}



// sub category


exports.SubCategoryAttributeFieldValue = function (req, res) {

  SuperCategory.findOne({ _id: req.params.supId }, function (err, category) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      var mainCat = category.mainCategory.id(req.params.maincatId)
      var subCat = mainCat.subCategory.id(req.params.subcatId);
      var subCatAtt = subCat.attribute.id(req.params.attributeId);
      var subCatfieldValue = subCatAtt.fieldValue.id(req.params.fieldId);
      subCatfieldValue.fieldAttributeValue = req.body.fieldAttributeValue;
      category.save(function (err, result) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
          SuperCategory.findOne({ _id: req.params.supId }, function (err, finddata) {
            if (err) {
              res.status(500).json(err);
            } else {
              var mainCat = finddata.mainCategory.id(req.params.maincatId);
              var subCat = mainCat.subCategory.id(req.params.subcatId);
              res.status(200).json(subCat);
            }
          })
        }
      });
    }
  });
}

exports.SubCategoryAttributeFieldValueDelete = function (req, res) {
  SuperCategory.findOne({ _id: req.params.supId }, function (err, category) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {

      var mainCat = category.mainCategory.id(req.params.maincatId);
      var subCat = mainCat.subCategory.id(req.params.subcatId);
      var subCatAtt = subCat.attribute.id(req.params.attributeId);
      subCatAtt.fieldValue.remove(req.params.fieldId);
      category.save(function (err, result) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
          SuperCategory.findOne({ _id: req.params.supId }).select().exec(function (err, finddata) {
            if (err) {
              res.status(500).json(err);
            } else {
              var mainCat = finddata.mainCategory.id(req.params.maincatId);
              var subCat = mainCat.subCategory.id(req.params.subcatId);
              res.status(200).json(subCat);
            }
          })
        }
      });
    }
  });
}

exports.SubCategoryAttribute = function (req, res) {
  SuperCategory.findOne({ _id: req.params.supId }, function (err, category) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      var mainCat = category.mainCategory.id(req.params.maincatId)
      var subCat = mainCat.subCategory.id(req.params.subcatId);
      var subCatAtt = subCat.attribute.id(req.params.attributeId);
      subCatAtt.fieldName = req.body.fieldName;
      subCatAtt.fieldType = req.body.fieldType;
      subCatAtt.fieldSetting = req.body.fieldSetting;
      subCatAtt.fieldEnable = req.body.fieldEnable;
      subCatAtt.fieldEnableValue = req.body.fieldEnableValue;
      subCatAtt.sortOrder = req.body.sortOrder;
      category.save(function (err, finddata) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
          var mainCat = finddata.mainCategory.id(req.params.maincatId);
          var subCat = mainCat.subCategory.id(req.params.subcatId);
          res.status(200).json(subCat);
        }
      });
    }
  });
}

exports.SubCategoryAttributeDelete = function (req, res) {

  SuperCategory.findOne({ _id: req.params.supId }, function (err, category) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      var mainCat = category.mainCategory.id(req.params.maincatId)
      var subCat = mainCat.subCategory.id(req.params.subcatId);
      var subCatAtt = subCat.attribute.id(req.params.attributeId);
      subCatAtt.attribute.remove(req.params.attributeId);
      category.save(function (err, finddata) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
          var mainCat = finddata.mainCategory.id(req.params.mainCatId);
          var subCat = mainCat.subCategory.id(req.params.subcatId);
          res.status(200).json(subCat);
        }
      });
    }
  });
}

exports.SubCategoryAttributeDelete = function (req, res) {

  SuperCategory.findOne({ _id: req.params.supId }, function (err, category) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      var mainCat = category.mainCategory.id(req.params.maincatId);
      var subCat = mainCat.subCategory.id(req.params.subcatId);
      subCat.attribute.remove(req.params.attributeId);
      category.save(function (err, finddata) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
          var mainCat = finddata.mainCategory.id(req.params.maincatId);
          var subCat = mainCat.subCategory.id(req.params.subcatId);
          res.status(200).json(subCat);
        }
      });
    }
  });
}


    exports.SubCategoryAttributeAdd = function (req, res) {
  var attributeData = {
    attribute: req.body.attribute
  };
  SuperCategory.findOne({
    _id: req.params.supId
  }, function (err, category) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      var mainCat = category.mainCategory.id(req.params.maincatId);
      var subCat = mainCat.subCategory.id(req.params.subcatId);
      attributeData.attribute.forEach(att => subCat.attribute.push(att));
      category.save(function (err, finddata) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
          var mainCat = finddata.mainCategory.id(req.params.maincatId);
          var subCat = mainCat.subCategory.id(req.params.subcatId);
          res.status(200).json(subCat);
        }
      });
    }
  });
}

exports.SubCategoryAttributeFieldValueAdd = function (req, res) {
  var attributeData = {
    fieldValue: req.body.fieldValue
  };
  /* SuperCategory.findOneAndUpdate({
    _id: req.params.supId, "mainCategory._id": req.params.maincatId, "subCategory._id": req.params.subcatId, 'attribute._id': req.params.attributeId
  }, {
    $push: {
      'attribute.$.fieldValue': attributeData.fieldValue
    }
  }, function (err, find) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      SubCategory.findOne({ _id: req.params.supId }).select().exec(function (err, finddata) {
        if (err) {
          res.status(500).json(err);
        } else {
          var mainCat = finddata.mainCategory.id(req.params.maincatId);
          var subCat = mainCat.subCategory.id(req.params.subcatId);
          res.status(200).json(subCat);
        }
      })
    }
  }); */
  SuperCategory.findOne({
    _id: req.params.supId
  }, function (err, category) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      var mainCat = category.mainCategory.id(req.params.maincatId);
      var subCat = mainCat.subCategory.id(req.params.subcatId);
      var attributeCat = subCat.attribute.id(req.params.attributeId);
      if(!attributeCat){
        attributeCat.fieldValue = attributeData.fieldValue;
      }  else {
        attributeData.fieldValue.forEach(att => attributeCat.fieldValue.push(att));
      }
      category.save(function (err, finddata) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
          var mainCat = finddata.mainCategory.id(req.params.maincatId);
          var subCat = mainCat.subCategory.id(req.params.subcatId);
          res.status(200).json(subCat);
        }
      });
    }
  });
}

// main category


exports.MainCategoryAttributeFieldValue = function (req, res) {
  SuperCategory.findOne({ _id: req.params.supId }, function (err, category) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      var mainCat = category.mainCategory.id(req.params.maincatId)
      var mainCatAtt = mainCat.attribute.id(req.params.attributeId);
      var mainCatfieldValue = mainCatAtt.fieldValue.id(req.params.fieldId);
      mainCatfieldValue.fieldAttributeValue = req.body.fieldAttributeValue;
      category.save(function (err, result) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
          SuperCategory.findOne({ _id: req.params.supId }, function (err, finddata) {
            if (err) {
              res.status(500).json(err);
            } else {
              var mainCat = finddata.mainCategory.id(req.params.maincatId);
              /* var subCat = mainCat.subCategory.id(req.params.subcatId); */
              res.status(200).json(mainCat);
            }
          })
        }
      });
    }
  });
}

exports.MainCategoryAttributeFieldValueDelete = function (req, res) {
  SuperCategory.findOne({ _id: req.params.supId }, function (err, category) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {

      var mainCat = category.mainCategory.id(req.params.maincatId)
      /* var subCat = mainCat.subCategory.id(req.params.subcatId); */
      var mainCatAtt = mainCat.attribute.id(req.params.attributeId);
      mainCatAtt.fieldValue.remove(req.params.fieldId);
      category.save(function (err, result) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
          /* SuperCategory.findOne({ _id: req.params.supId }).select().exec(function (err, finddata) {
            if (err) {
              res.status(500).json(err);
            } else {
              var mainCat = finddata.mainCategory.id(req.params.maincatId);
              
              res.status(200).json(mainCat);
            }
          }) */
        }
      });
    }
  });
}

exports.MainCategoryAttribute = function (req, res) {
  SuperCategory.findOne({ _id: req.params.supId }, function (err, category) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      var mainCat = category.mainCategory.id(req.params.maincatId)
      /* var subCat = mainCat.subCategory.id(req.params.subcatId); */
      var mainCatAtt = mainCat.attribute.id(req.params.attributeId);
      mainCatAtt.fieldName = req.body.fieldName;
      mainCatAtt.fieldType = req.body.fieldType;
      mainCatAtt.fieldSetting = req.body.fieldSetting;
      mainCatAtt.fieldEnable = req.body.fieldEnable;
      mainCatAtt.fieldEnableValue = req.body.fieldEnableValue;
      category.save(function (err, finddata) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
          var mainCat = finddata.mainCategory.id(req.params.maincatId);
          /* var subCat = mainCat.subCategory.id(req.params.subcatId); */
          res.status(200).json(mainCat);
        }
      });
    }
  });
}

exports.MainCategoryAttributeDelete = function (req, res) {

  SuperCategory.findOne({ _id: req.params.supId }, function (err, category) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      var mainCat = category.mainCategory.id(req.params.maincatId)
      var mainCatAtt = mainCat.attribute.id(req.params.attributeId);
      mainCatAtt.attribute.remove(req.params.attributeId);
      category.save(function (err, finddata) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
          var mainCat = finddata.mainCategory.id(req.params.mainCatId);
          /* var subCat = mainCat.subCategory.id(req.params.subcatId); */
          res.status(200).json(mainCat);
        }
      });
    }
  });
}

exports.MainCategoryAttributeDelete = function (req, res) {

  SuperCategory.findOne({ _id: req.params.supId }, function (err, category) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      var mainCat = category.mainCategory.id(req.params.maincatId);
      /* var subCat = mainCat.subCategory.id(req.params.subcatId); */
      mainCat.attribute.remove(req.params.attributeId);
      category.save(function (err, finddata) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
          var mainCat = finddata.mainCategory.id(req.params.maincatId);
          /* var subCat = mainCat.subCategory.id(req.params.subcatId); */
          res.status(200).json(mainCat);
        }
      });
    }
  });
}


    exports.MainCategoryAttributeAdd = function (req, res) {
  var attributeData = {
    attribute: req.body.attribute
  };
  SuperCategory.findOne({
    _id: req.params.supId
  }, function (err, category) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      var mainCat = category.mainCategory.id(req.params.maincatId);
      /* var subCat = mainCat.subCategory.id(req.params.subcatId); */
      attributeData.attribute.forEach(att => mainCat.attribute.push(att));
      category.save(function (err, finddata) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
          var mainCat = finddata.mainCategory.id(req.params.maincatId);
          
          res.status(200).json(mainCat);
        }
      });
    }
  });
}

exports.MainCategoryAttributeFieldValueAdd = function (req, res) {
  var attributeData = {
    fieldValue: req.body.fieldValue
  };
  /* SuperCategory.findOneAndUpdate({
    _id: req.params.supId, "mainCategory._id": req.params.maincatId, "subCategory._id": req.params.subcatId, 'attribute._id': req.params.attributeId
  }, {
    $push: {
      'attribute.$.fieldValue': attributeData.fieldValue
    }
  }, function (err, find) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      SubCategory.findOne({ _id: req.params.supId }).select().exec(function (err, finddata) {
        if (err) {
          res.status(500).json(err);
        } else {
          var mainCat = finddata.mainCategory.id(req.params.maincatId);
          var subCat = mainCat.subCategory.id(req.params.subcatId);
          res.status(200).json(subCat);
        }
      })
    }
  }); */
  SuperCategory.findOne({
    _id: req.params.supId
  }, function (err, category) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      var mainCat = category.mainCategory.id(req.params.maincatId);
      /* var subCat = mainCat.subCategory.id(req.params.subcatId); */
      var attributeCat = mainCat.attribute.id(req.params.attributeId);
      if(!attributeCat) {
        attributeCat.fieldValue = attributeData.fieldValue;
      }  else {
        attributeData.fieldValue.forEach(att => attributeCat.fieldValue.push(att));
      }
      category.save(function (err, finddata) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
          var mainCat = finddata.mainCategory.id(req.params.maincatId);
          /* var subCat = mainCat.subCategory.id(req.params.subcatId); */
          res.status(200).json(mainCat);
        }
      });
    }
  });
}