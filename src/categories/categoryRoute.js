'use strict';
var superCategoryMgr = require('./superCategory/superCategoryMgr');
var mainCategoryMgr = require('./mainCategory/mainCategoryMgr');
var subCategoryMgr = require('./subCategory/subCategoryMgr');
var attributeEditMgr = require('./attributeEdit/attributeEditMgr');
var categoryImageMgr = require('./addCategoryImage/categoryImageMgr');
var upload = require('../config/multer.config');

module.exports = function (app) {
// Super Category Start
    app.route('/addCategory')
        .post(superCategoryMgr.superCategoryInsert);
        
    app.route('/viewcategorywithattribute')
    .get(superCategoryMgr.superCategoryWithAttribute);
    
    app.route('/viewCategory')
        .get(superCategoryMgr.superCategoryShow);
        app.route('/getsinglecategory/:selectId')
        .get(superCategoryMgr.superCategorySingle);
    app.route('/superCategory')
        .get(superCategoryMgr.showSuperCategory);
    app.route('/categoryDelete/:categoryId')
        .delete(superCategoryMgr.superCategoryDelete);
    app.route('/category/:id')
        .put(superCategoryMgr.superCategoryEdit);
    app.route('/getsupercategorybystatus')
        .get(superCategoryMgr.showSuperCategoryByStatus);
    app.route('/supercategory/:supcatid')
        .get(superCategoryMgr.getSuperCategory);
    app.route('/sup')
        .get(superCategoryMgr.supcat);
    app.route('/attributesuperfields/:supId')
    .get(superCategoryMgr.findSuperCategoryAttribute);
    app.route('/attributesubfields/:subId')
    .get(superCategoryMgr.findSubCategoryAttribute);
// Super Category End

  // Main Category Start
  app.route('/mainCategory')
    .post(mainCategoryMgr.mainCategoryInsert);
  app.route('/category/:categoryId/mainCategory/:mainCategoryId')
    .get(mainCategoryMgr.mainCategoryUpdate);
  app.route('/category/:catId/mainCategoryfind/:mainId')
    .get(mainCategoryMgr.getAllMainCategoryFind);
  app.route('/category/:categoryId/mainCategory/:mainCategoryId')
    .delete(mainCategoryMgr.mainCategoryDelete);
  app.route('/superCategorydetail/:id')
    .get(mainCategoryMgr.getMainCategory);
  app.route('/showMainCategory')
    .get(mainCategoryMgr.getAllMainCategory);
    
    app.route('/supercategory/:supId/maincategoryimage/:mainId')
    .put(categoryImageMgr.addMainCategoryImage); // Upload Main 
  // Main Category End

  // Sub Category Start
  app.route('/allsinglesubcategory/:id')
    .get(subCategoryMgr.findAllSubCategory);
  app.route('/subCategory/:superid/add/:mainid')
    .put(subCategoryMgr.subCategoryInsert);
    app.route('/showMainCategory')
    .get(mainCategoryMgr.getAllMainCategory);
  app.route('/category/:categoryId/mainCategory/:mainCategoryId/subCategory/:subCategoryId')
    .delete(subCategoryMgr.subCategoryDelete);

  app.route('/category/:categoryId/mainCategory/:mainCategoryId/subCategory/:subCategoryId')
    .put(subCategoryMgr.subCategoryUpdate);
    app.route('/getselectedsubcategory/:categoryId/:mainCategoryId')
    .get(subCategoryMgr.findSubCategory);
    app.route('/attributesubfields/:subId')
    .get(subCategoryMgr.findSubCategoryAttribute);
    

    app.route('/supercategoryimage/:id')
    .put(categoryImageMgr.uploadSuperCategoryImages); // Upload Super Category
    
    
    app.route('/subcategoryimage/:supId/add/:mainId/addValue/:subId')
    .put(categoryImageMgr.addSubCategoryImage); // Upload Sub Category Image    

    app.route('/supcategory/:categoryId/maincategory/:mainCategoryId/subcategory/:subCategoryId')
    .get(subCategoryMgr.findSingleSubCategory); // get Sub Category   

    app.route('/supercategory/:supId/updatemaincategory/:mainId')
    .put(mainCategoryMgr.updateMainCategory); // Update Sub Category
    app.route('/super/:supId/attribute/:attributeId/field/:fieldId')
    .put(attributeEditMgr.SupCategoryAttributeFieldValue); // Update Attribute Field Category
    app.route('/super/:supId/attribute/:attributeId/field/:fieldId')
    .delete(attributeEditMgr.SupCategoryAttributeFieldValueDelete); // Delete Attribute Field Category
    app.route('/super/:supId/attribute/:attributeId')
    .put(attributeEditMgr.SupCategoryAttribute); // Update Attribute Category
    app.route('/super/:supId/attribute/:attributeId')
    .delete(attributeEditMgr.SupCategoryAttributeDelete); // Delete Attribute Category
    app.route('/super/:supId')
    .put(attributeEditMgr.SupCategoryAttributeAdd); // Add  Attribute Category
    app.route('/super/:supId/addattribute/:attributeId')
    .put(attributeEditMgr.SupCategoryAttributeFieldValueAdd); // Add  Attribute Category
  
    // main category edit
    
    app.route('/super/:supId/maincat/:maincatId/attribute/:attributeId/field/:fieldId')
    .put(attributeEditMgr.MainCategoryAttributeFieldValue); // Update Attribute Field Category
    app.route('/super/:supId/maincat/:maincatId/attribute/:attributeId/field/:fieldId')
    .delete(attributeEditMgr.MainCategoryAttributeFieldValueDelete); // Delete Attribute Field Category
    app.route('/super/:supId/maincat/:maincatId/attribute/:attributeId')
    .put(attributeEditMgr.MainCategoryAttribute); // Update Attribute Category
    app.route('/super/:supId/maincat/:maincatId/attribute/:attributeId')
    .delete(attributeEditMgr.MainCategoryAttributeDelete); // Delete Attribute Category
    app.route('/super/:supId/maincat/:maincatId')
    .put(attributeEditMgr.MainCategoryAttributeAdd); // Add  Attribute Category
    app.route('/super/:supId/maincat/:maincatId/addattribute/:attributeId')
    .put(attributeEditMgr.MainCategoryAttributeFieldValueAdd); // Add  Attribute Category

 // sub category edit
 app.route('/super/:supId/maincat/:maincatId/subcat/:subcatId/attribute/:attributeId/field/:fieldId')
 .put(attributeEditMgr.SubCategoryAttributeFieldValue); // Update Attribute Field Category
 app.route('/super/:supId/maincat/:maincatId/subcat/:subcatId/attribute/:attributeId/field/:fieldId')
 .delete(attributeEditMgr.SubCategoryAttributeFieldValueDelete); // Delete Attribute Field Category
 app.route('/super/:supId/maincat/:maincatId/subcat/:subcatId/attribute/:attributeId')
 .put(attributeEditMgr.SubCategoryAttribute); // Update Attribute Category
 app.route('/super/:supId/maincat/:maincatId/subcat/:subcatId/attribute/:attributeId')
 .delete(attributeEditMgr.SubCategoryAttributeDelete); // Delete Attribute Category
 app.route('/super/:supId/maincat/:maincatId/subcat/:subcatId')
 .put(attributeEditMgr.SubCategoryAttributeAdd); // Add  Attribute Category
 app.route('/super/:supId/maincat/:maincatId/subcat/:subcatId/addattribute/:attributeId')
 .put(attributeEditMgr.SubCategoryAttributeFieldValueAdd); // Add  Attribute Category

  /*      app.route('/categoryDetails')
          .get(superCategoryMgr.superCategoryShow);

   
        app.route('/category/:id')
          .put(superCategoryMgr.superCategoryEdit);

      app.route('/categoryDelete/:categoryId/name/:name')
          .delete(superCategoryMgr.superCategoryDelete);

      app.route('/supercategoryimage/:id')
          .put(superCategoryMgr.createSuperCategoryImage);
   */
}
