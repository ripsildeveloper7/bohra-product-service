var categoryImageDA = require('./../addCategoryImage/categoryImageDA');
exports.uploadSuperCategoryImages = function (req, res) {
    try {
        categoryImageDA.uploadSuperCategoryImages(req,  res);
    } catch (error) {
      console.log(error);
    }
  }
  
  exports.addMainCategoryImage = function (req, res) {
    try {
        categoryImageDA.addMainCategoryImage(req, res);
     
    } catch (error) {
        console.log(error);
    }
  }
  
  exports.addSubCategoryImage = function (req, res) {
    try {
        categoryImageDA.addSubCategoryImage(req,res);
    } catch (error) {
        console.log(error);
    }
  }