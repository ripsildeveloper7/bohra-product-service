var SuperCategory = require('./../../model/superCategory.model');

exports.uploadSuperCategoryImages = function (req, res) {
  SuperCategory.findOne({
    '_id': req.params.id
  }, function (err, data) {
    if (err) {
      res.status(500).send({
        message: "Some error occurred while retrieving notes."
      });
    } else {
      data.categoryImageName = req.body.categoryImageName;
      data.save(function (err, superCategoryImageSave) {
        if (err) {
          res.status(500).send({
            message: 1
          });
        } else {
          SuperCategory.find({}).select().exec(function (err, data) {
            if (err) {
              res.status(500).send({
                message: "Some error occurred while retrieving notes."
              });
            } else {
              res.status(200).json(data);
            }
        });
        }
      });
    }
  });
}

exports.addMainCategoryImage = function (req,  res) {
  SuperCategory.findById(req.params.supId, function (err, category) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {
      var mainCat = category.mainCategory.id(req.params.mainId)
      mainCat.mainCategoryImageName = req.body.mainCategoryImageName;
      category.save(function (err, result) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
          SuperCategory.find({}).select().exec(function (err, finddata) {
            if (err) {
              res.status(500).json(err);
            } else {
              res.status(200).json(finddata);
            }
          });
        }
      });
    }
  });
}       

exports.addSubCategoryImage = function (req, res) {
  SuperCategory.findById(req.params.supId, function (err, category) {
    if (err) {
      res.status(500).send({
        "result": 0
      });
    } else {

      var mainCat = category.mainCategory.id(req.params.mainId)
      var subCat = mainCat.subCategory.id(req.params.subId);
      subCat.subCategoryImageName = req.body.subCategoryImageName;
      category.save(function (err, result) {
        if (err) {
          res.status(500).send({
            "result": 1
          });
        } else {
          res.status(200).json(result);
        }
      });
    }
  });
}