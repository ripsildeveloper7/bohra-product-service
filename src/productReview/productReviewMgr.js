var reviewDA = require('./productReviewDA');

exports.createReview = function(req, res) {
    try {
        reviewDA.createReview(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getSelectedProductReview = function(req, res) {
    try {
        reviewDA.getSelectedProductReview(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getProductReviewWithProduct = function(req, res) {
    try {
        reviewDA.getProductReviewWithProduct(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getSingleProductReviewWithProduct = function(req, res) {
    try {
        reviewDA.getSingleProductReviewWithProduct(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.deleteProductReview = function(req, res) {
    try {
        reviewDA.deleteProductReview(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.updatePublishProductReview = function(req, res) {
    try {
        reviewDA.updatePublishProductReview(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.UpdateUnPublishProductReview = function(req, res) {
    try {
        reviewDA.UpdateUnPublishProductReview(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getPublishedReview = function(req, res) {
    try {
        reviewDA.getPublishedReview(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getReviewForVerifyOrder = function(req, res) {
    try {
        reviewDA.getReviewForVerifyOrder(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getReviewByOrderID = function(req, res) {
    try {
        reviewDA.getReviewByOrderID(req, res);
    } catch (error) {
        console.log(error);
    }
}