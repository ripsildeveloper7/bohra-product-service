var reviewDetails = require('../model/productReview.model');
var ProductDetails = require('../model/product.model');
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;

exports.createReview = function(req, res) {
    var create = reviewDetails(req.body);
    create.createdDate = new Date();
    create.save(function(err, data) {
        if(err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(data);
        }
    })
}

exports.getSelectedProductReview = function(req, res) {
    reviewDetails.find({'productId': req.params.id}).select().exec(function(err, findData) {
        if(err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}

exports.getProductReviewWithProduct = function(req, res) {
    reviewDetails.aggregate([{
        $lookup: {
          from: "products",
          localField: 'productId',
          foreignField: '_id',
          as: "productdetails"
        }
      }
    ], function (err, aggData) {
      if (err) {
        res.status(500).json(err);
      } else {
        res.status(200).json(aggData);
      }
    })
}

exports.getSingleProductReviewWithProduct = function(req, res) {
    reviewDetails.aggregate([{
        $lookup: {
          from: "products",
          localField: 'productId',
          foreignField: '_id',
          as: "productdetails"
        }
      },
      {
        $match: {
            _id: ObjectId(req.params.id)
          }
      }
    ], function (err, aggData) {
      if (err) {
        res.status(500).json(err);
      } else {
        res.status(200).json(aggData);
      }
    })
}
exports.deleteProductReview = function(req, res) {
    reviewDetails.findOneAndDelete({'_id': req.params.id}).select().exec(function(err, data) {
        if(err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(data);
        }
    })
}

exports.updatePublishProductReview = function (req, res) {
    var second = [];
    for (let i = 0; i <= req.body.length - 1; i++) {
      first = req.body[i]._id;
      second.push(first);
    }
    reviewDetails.updateMany({
      '_id': second
    }, {
      $set: {
        'publish': true
      }
    }).select().exec(function (err, data) {
      if (err) {
        res.status(500).json(err);
      } else {
        reviewDetails.find({}).select().exec(function (err, data) {
          if (err) {
            res.status(500).send({
              message: "Some error occurred while retrieving notes."
            });
          } else {
            res.status(200).json(data);
          }
        });
      }
    })
  }
  
  exports.UpdateUnPublishProductReview = function (req, res) {
    var second = [];
    for (let i = 0; i <= req.body.length - 1; i++) {
      first = req.body[i]._id;
      second.push(first);
    }
    reviewDetails.updateMany({
      '_id': second
    }, {
      $set: {
        'publish': false
      }
    }).select().exec(function (err, data) {
      if (err) {
        res.status(500).json(err);
      } else {
        reviewDetails.findOne({
          '_id': req.params.id
        }).select().exec(function (err, data1) {
          if (err) {
            res.status(500).json(err);
          } else {
            reviewDetails.find({}).select().exec(function (err, data) {
              if (err) {
                res.status(500).send({
                  message: "Some error occurred while retrieving notes."
                });
              } else {
                res.status(200).json(data);
              }
            });
          }
        })
      }
    })
  }

  exports.getPublishedReview = function(req, res) {
    reviewDetails.find({'publish': true}).select().exec(function(err, review) {
      if (err) {
        res.status(500).json(err);
      } else {
        res.status(200).json(review);
      }
    })
  }

  exports.getReviewForVerifyOrder = function(req, res) {
    reviewDetails.find({}).select('orderId cartId').exec(function(err, review) {
      if (err) {
        res.status(500).json(err);
      } else {
        res.status(200).json(review);
    }
    })
  }
  
  exports.getReviewByOrderID = function(req, res) {
    reviewDetails.find({'orderId': req.params.id}).select().exec(function(err, review) {
      if (err) {
        res.status(500).json(err);
      } else {
        res.status(200).json(review);
    }
    })
  }