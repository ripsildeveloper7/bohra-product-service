var s3 = require('../config/s3.config');
var env = require('../config/s3.env');
const AWS = require('aws-sdk');



exports.uploadSingleImage = function (req, res) {
  const base64Data = new Buffer(req.body.brandImageName, 'base64');
  /* const type = req.body.brandImageName.split(';')[0].split('/')[1] */
  
  const params = {
    Bucket: env.Bucket + '/' + 'images' + '/' + 'brand' + '/' + req.params.id, // create a folder and save the image
    Key: req.body.brandName,
    ACL: 'public-read',
    ContentEncoding: 'base64',
    Body: base64Data,
    ContentType: `image/${type}`
  };
  console.log(params);
  /* s3.upload(params, function (err, data) {
    if (err) {
      console.log(err);
    } else {
        console.log(data);
      res.status(200).json(data);
    }
  }); */
}

exports.uploadSingleImageMulter = function (req, file, res) {
    console.log(file)
    res.status(200).json(file);
  
}


exports.uploadMultiImageMulter = function (req, file, res) {
    console.log(file)
    res.status(200).json(file);
}


exports.uploadExcel = function (req, res) {
  /* const base64Data = new Buffer(req.body.data.replace(/^data:application\vnd.ms-excel\/\w+;base64,/, ""), 'base64'); */
  /* const type = req.body.data.split(';')[0].split('/')[1] */
  /* const test = new Uint8Array(req.body);
  console.log(Buffer.from(test)); */
  const params = {
    Bucket: env.Bucket + '/' + 'images' + '/' + 'excel', // create a folder and save the image
    Key:  req.body.test.name,
    ACL: 'public-read',
    Body: req.body.test
  };

  s3.upload(params, function (err, data) {
    if (err) {
      console.log(err);
    } else {
        console.log(data);
      res.status(200).json(data);
    }
  });
}